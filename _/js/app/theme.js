// usage: log('inside coolFunc',this,arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function() {
	window.log.history = window.log.history || []; // store logs to an array for reference
	window.log.history.push(arguments);
	if (this.console) {
		window.console.log(Array.prototype.slice.call(arguments));
	}
};

/*jshint multistr: true */

/**
 * AppMain entrypoint.
 * Handles registering and initializing the modules.
 * Handles event pub/sub system for the modules.
 **/
var AppMain = (function($) {
	'use strict';

	var initialized = false,
		initializers = [],
		my = {
			$: $,
			settings: {
				templates:{
					punt: '<div class="punt-clone">&nbsp;</div>'
				}
			},
			modules: {

			}
		},
		o = $({}); // object to attach pubsub events to.
	my.subscribe = function() {
		o.bind.apply(o, arguments);
	};

	my.unsubscribe = function() {
		o.unbind.apply(o, arguments);
	};

	my.publish = function() {
		o.trigger.apply(o, arguments);
	};

	my.addInitializer = function(callback) {
		var initializer = {
			scope: this,
			callback: callback
		};
		if (!initialized) {
			initializers.push(initializer);
		} else {
			initializer.callback.call(initializer.scope);
		}
	};

	my.initialize = function() {
		$.each(initializers, function(i, initializer) {
			initializer.callback.call(initializer.scope);
		});
		my.publish('after.initialize');
		// fire an event when all modules are initialized.
		initialized = true;
	};
	return my;
}(jQuery));

/**
 * Punt module.
 * - Achtergrond videos
 */
var PuntModule = (function(app) {
	var my = {},
		$,
		$container,
		midPoint,
		punten = "";
	my.name = "punt";
	my.version = 1;

	function moduleInit() {
		$ = app.$;
		app.subscribe('after.initialize', _onInitializationDone);
		_createContainer();
		_onResize();
		_loadPunten();
	}

	var _onResize = function ( ){
		var w = $( window ).width();
		midPoint = w / 2;
		$container.hide();
		$container.width( $( document ).width() );
		$container.height( $( document ).height() );
		$container.show();
		_redrawPunten();
	}

	var _onClick = function( e ){
		var x = e.pageX;
			y = e.pageY;
		var dx = x - midPoint;
		// _appendDot( dx, y, 0, 1 );
		_storePunt( dx, y );
	}

	// var mouseDownTime;
	// var _onMouseDown = function( e ){
	// 	var timerDate = new Date();
	// 	mouseDownTime = timerDate.getTime();
	// 	log( mouseDownTime );
	// }
	// var _onMouseUp = function( e ){
	// 	var timerDate = new Date();
	// 	var mouseUpTime = timerDate.getTime(),
	// 		t = mouseUpTime - mouseDownTime,
	// 		x = e.pageX;
	// 		y = e.pageY;
	// 	_appendDot( x, y, t / 10 );
	// 	_storePunt( x, y );
	// }


	var _createContainer = function(){
		$container = $( '<div></div>', {
			'class': "punt-container",
			'css': {

			}
		} );
		$( 'body' ).prepend( $container );
	}

	var _loadPunten = function(){
		var data = {
			'version': my.version,
			'mode': 'get'
		}
		$.getJSON( 'punten.php', data, function( data ) {
			if ( data.punten ){
				punten = data.punten;
				_drawPunten();
			}
		});
	}

	var _drawPunten = function(){
		var coord;
		var p = punten.split( '|' );
		var punt_count = p.length;
		$.each( p, function( i, coord_str ){
			coord = coord_str.split( ',' );
			log( coord_str );
			if ( coord.length == 2 ){
				var opacity = i / punt_count;
				_appendDot( coord[ 0 ], coord[ 1 ], punt_count - i, opacity );
			}
		} );
	}

	var _redrawPunten = function( data ){
		_removePunten();
		_drawPunten( data );
	}

	var _removePunten = function ( ){
		$( '.punt-clone' ).remove();
	}

	var _appendDot = function( dx, y, idx, opacity ){
		dx = parseInt( dx, 10 );
		var rot = ( dx + y ) % 360,
			punt = $( app.settings.templates.punt ).attr( 'class', 'punt-clone druppel50p-drup' + zeroFill( idx + 1, 3 ) );

		$container.append(
			punt
		);
		log( punt.css( 'width' ) );
		punt.css( {
			top: parseInt( y ),
			left: midPoint + dx,
			// width: size + 'px',
			// height: size + 'px',
			marginLeft: punt.width()/-2,
			marginTop: punt.height()/-2,
			transform: 'rotate(' + rot + 'deg) scale( '+( 0.2 + 1 - opacity)+ ')',
			opacity: opacity
		} );
	}

	function zeroFill( number, width ){
		width -= number.toString().length;
		if ( width > 0 ){
			return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
		}
		return number + ""; // always return a string
	}

	var _storePunt = function ( x , y ){
		var query = {
			'mode': 'put',
			'version': my.version,
			'punt': Math.round( x ) + ',' + Math.round( y )
		}
		$.getJSON( 'punten.php', query, function( data ) {
			if ( data.error ) log( "error saving punt. " );
			if ( data.punten ){
				punten = data.punten
				_redrawPunten();
			}
		});
	}

	var _onInitializationDone = function() {
		$( document ).on( 'click', _onClick );
		$( window ).on( 'resize', _onResize );
			// .on( 'mousedown', _onMouseDown )
			// .on( 'mouseup', _onMouseUp );
	};

	app.addInitializer(moduleInit);
	return my;
}(AppMain));

// Start it all up.
jQuery(function() {
	'use strict';
	AppMain.initialize();
});