/*
 * hScroll - Plugin for Jquery that controls horizontal scrolling
 *
 * Copyright (c) 2010 Heerko van der Kooij (swummoq.net)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 */

;
(function($, window, document, undefined) {
	var pluginName = 'browseList',
		defaults = {
			'page_size': 3,
			'element_selector': 'li',
			'loop': true,
			'onInitDone': null,
			'beforeElementDisplay': null,
			'onDisplayDone': null
		};

	function Plugin(element, opts) {
		this.element = $(element);
		this.options = $.extend({}, defaults, opts);
		this.page = 0;
		this.pages = 0;
		this.els = [];
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	Plugin.prototype.init = function() {
		this.els = this.element.find(this.options.element_selector);
		this.pages = Math.ceil(this.els.length / this.options.page_size);
		this.displayCurrentPage();
		if ( 'function' == typeof this.options.onInitDone ){
			this.options.onInitDone.call( this, this, this.element, this.els );
		}
	}

	Plugin.prototype.displayCurrentPage = function() {
		this.els.hide();
		var start = this.page * this.options.page_size,
			end = Math.min(start + this.options.page_size, this.els.length);
		for (i = start; i < end; i++) {
			var el = $(this.els[i]);
			if ( ! el.data( pluginName + '_inited' ) ){
				el.data( pluginName + '_inited', true );
				if ( 'function' == typeof this.options.beforeElementDisplay ){
					this.options.beforeElementDisplay.call( this, el );
				}
			}
			el.fadeIn();
		}
		if ( 'function' == typeof this.options.onDisplayDone ){
			this.options.onDisplayDone.call( this, this, this.element, this.els );
		}
	}

	Plugin.prototype.next = function() {
		if (this.page >= this.pages - 1) {
			if (this.options.loop) {
				this.page = 0;
			} else {
				return -1;
			}
		} else this.page++;
		this.displayCurrentPage();
		return ( this.page < this.pages - 1 || this.options.loop );
	}

	Plugin.prototype.prev = function() {
		if (this.page <= 0) {
			if (this.options.loop) {
				this.page = this.pages - 1;
			} else {
				return false;
			}
		} else this.page--;
		this.displayCurrentPage();
		return ( this.page > 0 || this.options.loop );
	}

	Plugin.prototype.jumptoItem = function( showItem ) {
		var newpage = Math.floor( ( showItem - 1 ) / this.options.page_size );
		if ( newpage != this.page ){
			this.page = newpage;
			this.displayCurrentPage();
		}
	}

	Plugin.prototype.getState = function() {
		return state = {
			totalPages: this.pages,
			currentPage: this.page,
			hasNext: ( this.page < this.pages - 1 || this.options.loop ),
			hasPrev: ( this.page > 0 || this.options.loop ),
			loop: this.loop
		}
	}

	$.fn[pluginName] = function(args) {
		return this.each(function() {
			var inst, options = args;
			if (typeof args === 'object' || undefined === args) {
				if (!$.data(this, 'plugin_' + pluginName)) {
					inst = new Plugin(this, options);
					$.data(this, 'plugin_' + pluginName, inst);
				}
			} else if (typeof args === 'string' && Plugin.prototype[args]) {
				if (!$.data(this, 'plugin_' + pluginName)) {
					$.error('Initialize the plugin before calling "' + args + '"');
					return;
				}
				inst = $.data(this, 'plugin_' + pluginName);
				return inst[args].apply(inst, Array.prototype.slice.call(arguments, 1));
			} else {
				$.error('Method ' + args + ' does not exist on jQuery.' + pluginName);
			}
		});
	};

}(jQuery, window, document));