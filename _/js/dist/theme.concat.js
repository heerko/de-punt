/*! Some Project - v1 - 2013-01-22
* http://www.jvdwfilm.nl/
* Copyright (c) 2013 Heerko van der Kooij; Licensed MIT */

/*!
	AnythingSlider v1.8.7
	Original by Chris Coyier: http://css-tricks.com
	Get the latest version: https://github.com/CSS-Tricks/AnythingSlider

	To use the navigationFormatter function, you must have a function that
	accepts two paramaters, and returns a string of HTML text.

	index = integer index (1 based);
	panel = jQuery wrapped LI item this tab references
	@return = Must return a string of HTML/Text

	navigationFormatter: function(index, panel){
		return "Panel #" + index; // This would have each tab with the text 'Panel #X' where X = index
	}
*/
/*jshint browser:true, jquery:true, unused:false */
;(function($, win, doc) {
	"use strict";
	$.anythingSlider = function(el, options) {

		var base = this, o, t;

		// Wraps the ul in the necessary divs and then gives Access to jQuery element
		base.el = el;
		base.$el = $(el).addClass('anythingBase').wrap('<div class="anythingSlider"><div class="anythingWindow" /></div>');

		// Add a reverse reference to the DOM object
		base.$el.data("AnythingSlider", base);

		base.init = function(){

			// Added "o" to be used in the code instead of "base.options" which doesn't get modifed by the compiler - reduces size by ~1k
			base.options = o = $.extend({}, $.anythingSlider.defaults, options);

			base.initialized = false;
			if ($.isFunction(o.onBeforeInitialize)) { base.$el.bind('before_initialize', o.onBeforeInitialize); }
			base.$el.trigger('before_initialize', base);

			// Add "as-oldie" class to body for css purposes
			$('<!--[if lte IE 8]><script>jQuery("body").addClass("as-oldie");</script><![endif]-->').appendTo('body').remove();

			// Cache existing DOM elements for later
			// base.$el = original ul
			// for wrap - get parent() then closest in case the ul has "anythingSlider" class
			base.$wrapper = base.$el.parent().closest('div.anythingSlider').addClass('anythingSlider-' + o.theme);
			base.$outer = base.$wrapper.parent();
			base.$window = base.$el.closest('div.anythingWindow');
			base.win = win;
			base.$win = $(base.win);

			base.$controls = $('<div class="anythingControls"></div>');
			base.$nav = $('<ul class="thumbNav"><li><a><span></span></a></li></ul>');
			base.$startStop = $('<a href="#" class="start-stop"></a>');
			
			if (o.buildStartStop || o.buildNavigation) {
				base.$controls.appendTo( (o.appendControlsTo && $(o.appendControlsTo).length) ? $(o.appendControlsTo) : base.$wrapper);
			}
			if (o.buildNavigation) {
				base.$nav.appendTo( (o.appendNavigationTo && $(o.appendNavigationTo).length) ? $(o.appendNavigationTo) : base.$controls );
			}
			if (o.buildStartStop) {
				base.$startStop.appendTo( (o.appendStartStopTo && $(o.appendStartStopTo).length) ? $(o.appendStartStopTo) : base.$controls );
			}

			// Figure out how many sliders are on the page for indexing
			base.runTimes = $('.anythingBase').length;
			// hash tag regex - fixes issue #432
			base.regex = (o.hashTags) ? new RegExp('panel' + base.runTimes + '-(\\d+)', 'i') : null;
			if (base.runTimes === 1) { base.makeActive(); } // make the first slider on the page active

			// Set up a few defaults & get details
			base.flag    = false; // event flag to prevent multiple calls (used in control click/focusin)
			if (o.autoPlayLocked) { o.autoPlay = true; } // if autoplay is locked, start playing
			base.playing = o.autoPlay; // slideshow state; removed "startStopped" option
			base.slideshow = false; // slideshow flag needed to correctly trigger slideshow events
			base.hovered = false; // actively hovering over the slider
			base.panelSize = [];  // will contain dimensions and left position of each panel
			base.currentPage = base.targetPage = o.startPanel = parseInt(o.startPanel,10) || 1; // make sure this isn't a string
			o.changeBy = parseInt(o.changeBy,10) || 1;

			// set slider type, but keep backward compatibility with the vertical option
			t = (o.mode || 'h').toLowerCase().match(/(h|v|f)/);
			t = o.vertical ? 'v' : (t || ['h'])[0];
			o.mode = t === 'v' ? 'vertical' : t === 'f' ? 'fade' : 'horizontal';
			if (t === 'f') {
				o.showMultiple = 1; // all slides are stacked in fade mode
				o.infiniteSlides = false; // no cloned slides
			}

			base.adj = (o.infiniteSlides) ? 0 : 1; // adjust page limits for infinite or limited modes
			base.adjustMultiple = 0;
			if (o.playRtl) { base.$wrapper.addClass('rtl'); }

			// Build start/stop button
			if (o.buildStartStop) { base.buildAutoPlay(); }

			// Build forwards/backwards buttons
			if (o.buildArrows) { base.buildNextBackButtons(); }

			base.$lastPage = base.$targetPage = base.$currentPage;

			base.updateSlider();

			// Expand slider to fit parent
			if (o.expand) {
				base.$window.css({ width: '100%', height: '100%' }); // needed for Opera
				base.checkResize();
			}

			// Make sure easing function exists.
			if (!$.isFunction($.easing[o.easing])) { o.easing = "swing"; }

			// If pauseOnHover then add hover effects
			if (o.pauseOnHover) {
				base.$wrapper.hover(function() {
					if (base.playing) {
						base.$el.trigger('slideshow_paused', base);
						base.clearTimer(true);
					}
				}, function() {
					if (base.playing) {
						base.$el.trigger('slideshow_unpaused', base);
						base.startStop(base.playing, true);
					}
				});
			}

			// Hide/Show navigation & play/stop controls
			base.slideControls(false);
			base.$wrapper.bind('mouseenter mouseleave', function(e){
				// add hovered class to outer wrapper
				$(this)[e.type === 'mouseenter' ? 'addClass' : 'removeClass']('anythingSlider-hovered');
				base.hovered = (e.type === 'mouseenter') ? true : false;
				base.slideControls(base.hovered);
			});

			// Add keyboard navigation
			$(doc).keyup(function(e){
				// Stop arrow keys from working when focused on form items
				if (o.enableKeyboard && base.$wrapper.hasClass('activeSlider') && !e.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
					if (o.mode !== 'vertical' && (e.which === 38 || e.which === 40)) { return; }
					switch (e.which) {
						case 39: case 40: // right & down arrow
							base.goForward();
							break;
						case 37: case 38: // left & up arrow
							base.goBack();
							break;
					}
				}
			});

			// If a hash can not be used to trigger the plugin, then go to start panel
			base.currentPage = base.gotoHash() || o.startPanel || 1;
			base.gotoPage(base.currentPage, false, null, -1);

			// Binds events
			var triggers = "slideshow_paused slideshow_unpaused slide_init slide_begin slideshow_stop slideshow_start initialized swf_completed".split(" ");
			$.each("onShowPause onShowUnpause onSlideInit onSlideBegin onShowStop onShowStart onInitialized onSWFComplete".split(" "), function(i,f){
				if ($.isFunction(o[f])){
					base.$el.bind(triggers[i], o[f]);
				}
			});
			if ($.isFunction(o.onSlideComplete)){
				// Added setTimeout (zero time) to ensure animation is complete... see this bug report: http://bugs.jquery.com/ticket/7157
				base.$el.bind('slide_complete', function(){
					setTimeout(function(){ o.onSlideComplete(base); }, 0);
					return false;
				});
			}
			base.initialized = true;
			base.$el.trigger('initialized', base);

			// trigger the slideshow
			base.startStop(o.autoPlay);

		};

		// called during initialization & to update the slider if a panel is added or deleted
		base.updateSlider = function(){
			// needed for updating the slider
			base.$el.children('.cloned').remove();
			base.navTextVisible = base.$nav.find('span:first').css('visibility') !== 'hidden';
			base.$nav.empty();
			// set currentPage to 1 in case it was zero - occurs when adding slides after removing them all
			base.currentPage = base.currentPage || 1;

			base.$items = base.$el.children();
			base.pages = base.$items.length;
			base.dir = (o.mode === 'vertical') ? 'top' : 'left';
			o.showMultiple = (o.mode === 'vertical') ? 1 : parseInt(o.showMultiple,10) || 1; // only integers allowed
			o.navigationSize = (o.navigationSize === false) ? 0 : parseInt(o.navigationSize,10) || 0;

			// Fix tabbing through the page, but don't change the view if the link is in view (showMultiple = true)
			base.$items.find('a').unbind('focus.AnythingSlider').bind('focus.AnythingSlider', function(e){
				var panel = $(this).closest('.panel'),
					indx = base.$items.index(panel) + base.adj; // index can be -1 in nested sliders - issue #208
				base.$items.find('.focusedLink').removeClass('focusedLink');
				$(this).addClass('focusedLink');
				base.$window.scrollLeft(0).scrollTop(0);
				if ( ( indx !== -1 && (indx >= base.currentPage + o.showMultiple || indx < base.currentPage) ) ) {
					base.gotoPage(indx);
					e.preventDefault();
				}
			});
			if (o.showMultiple > 1) {
				if (o.showMultiple > base.pages) { o.showMultiple = base.pages; }
				base.adjustMultiple = (o.infiniteSlides && base.pages > 1) ? 0 : o.showMultiple - 1;
			}

			// Hide navigation & player if there is only one page
			base.$controls
				.add(base.$nav)
				.add(base.$startStop)
				.add(base.$forward)
				.add(base.$back)[(base.pages <= 1) ? 'hide' : 'show']();
			if (base.pages > 1) {
				// Build/update navigation tabs
				base.buildNavigation();
			}

			// Top and tail the list with 'visible' number of items, top has the last section, and tail has the first
			// This supports the "infinite" scrolling, also ensures any cloned elements don't duplicate an ID
			// Moved removeAttr before addClass otherwise IE7 ignores the addClass: http://bugs.jquery.com/ticket/9871
			if (o.mode !== 'fade' && o.infiniteSlides && base.pages > 1) {
				base.$el.prepend( base.$items.filter(':last').clone().addClass('cloned') );
				// Add support for multiple sliders shown at the same time
				if (o.showMultiple > 1) {
					base.$el.append( base.$items.filter(':lt(' + o.showMultiple + ')').clone().addClass('cloned multiple') );
				} else {
					base.$el.append( base.$items.filter(':first').clone().addClass('cloned') );
				}
				base.$el.find('.cloned').each(function(){
					// disable all focusable elements in cloned panels to prevent shifting the panels by tabbing
					$(this).find('a,input,textarea,select,button,area,form').attr({ disabled : 'disabled', name : '' });
					$(this).find('[id]').andSelf().removeAttr('id');
				});
			}

			// We just added two items, time to re-cache the list, then get the dimensions of each panel
			base.$items = base.$el.addClass(o.mode).children().addClass('panel');
			base.setDimensions();

			// Set the dimensions of each panel
			if (o.resizeContents) {
				base.$items.css('width', base.width);
				base.$wrapper
					.css('width', base.getDim(base.currentPage)[0])
					.add(base.$items).css('height', base.height);
			} else {
				base.$win.load(function(){
					// set dimensions after all images load
					base.setDimensions();
					// make sure the outer wrapper is set properly
					t = base.getDim(base.currentPage);
					base.$wrapper.css({ width: t[0], height: t[1] });
					base.setCurrentPage(base.currentPage, false);
				});
			}

			if (base.currentPage > base.pages) {
				base.currentPage = base.pages;
			}
			base.setCurrentPage(base.currentPage, false);
			base.$nav.find('a').eq(base.currentPage - 1).addClass('cur'); // update current selection

			if (o.mode === 'fade') {
				var t = base.$items.eq(base.currentPage-1);
				if (o.resumeOnVisible) {
					// prevent display: none;
					t.css({ opacity: 1 }).siblings().css({ opacity: 0 });
				} else {
					// allow display: none; - resets video
					base.$items.css('opacity',1);
					t.fadeIn(0).siblings().fadeOut(0);
				}
			}

		};

		// Creates the numbered navigation links
		base.buildNavigation = function() {
			if (o.buildNavigation && (base.pages > 1)) {
				var a, c, i, t, $li;
				base.$items.filter(':not(.cloned)').each(function(j){
					$li = $('<li/>');
					i = j + 1;
					c = (i === 1 ? ' first' : '') + (i === base.pages ? ' last' : '');
					a = '<a class="panel' + i + ( base.navTextVisible ? '"' : ' ' + o.tooltipClass + '" title="@"' ) + ' href="#"><span>@</span></a>';
					// If a formatter function is present, use it
					if ($.isFunction(o.navigationFormatter)) {
						t = o.navigationFormatter(i, $(this));
						if (typeof(t) === "string") {
							$li.html(a.replace(/@/g,t));
						} else {
							$li = $('<li/>', t);
						}
					} else {
						$li.html(a.replace(/@/g,i));
					}
					$li
					.appendTo(base.$nav)
					.addClass(c)
					.data('index', i);
				});
				base.$nav.children('li').bind(o.clickControls, function(e) {
					if (!base.flag && o.enableNavigation) {
						// prevent running functions twice (once for click, second time for focusin)
						base.flag = true; setTimeout(function(){ base.flag = false; }, 100);
						base.gotoPage( $(this).data('index') );
					}
					e.preventDefault();
				});

				// Add navigation tab scrolling - use !! in case someone sets the size to zero
				if (!!o.navigationSize && o.navigationSize < base.pages) {
					if (!base.$controls.find('.anythingNavWindow').length){
						base.$nav
							.before('<ul><li class="prev"><a href="#"><span>' + o.backText + '</span></a></li></ul>')
							.after('<ul><li class="next"><a href="#"><span>' + o.forwardText + '</span></a></li></ul>')
							.wrap('<div class="anythingNavWindow"></div>');
					}
					// include half of the left position to include extra width from themes like tabs-light and tabs-dark (still not perfect)
					base.navWidths = base.$nav.find('li').map(function(){
						return $(this).outerWidth(true) + Math.ceil(parseInt($(this).find('span').css('left'),10)/2 || 0);
					}).get();
					base.navLeft = base.currentPage;
					// add 25 pixels (old IE needs more than 5) to make sure the tabs don't wrap to the next line
					base.$nav.width( base.navWidth( 1, base.pages + 1 ) + 25 );
					base.$controls.find('.anythingNavWindow')
						.width( base.navWidth( 1, o.navigationSize + 1 ) ).end()
						.find('.prev,.next').bind(o.clickControls, function(e) {
							if (!base.flag) {
								base.flag = true; setTimeout(function(){ base.flag = false; }, 200);
								base.navWindow( base.navLeft + o.navigationSize * ( $(this).is('.prev') ? -1 : 1 ) );
							}
							e.preventDefault();
						});
				}

			}
		};

		base.navWidth = function(x,y){
			var i, s = Math.min(x,y),
				e = Math.max(x,y),
				w = 0;
			for (i = s; i < e; i++) {
				w += base.navWidths[i-1] || 0;
			}
			return w;
		};

		base.navWindow = function(n){
			if (!!o.navigationSize && o.navigationSize < base.pages && base.navWidths) {
				var p = base.pages - o.navigationSize + 1;
				n = (n <= 1) ? 1 : (n > 1 && n < p) ? n : p;
				if (n !== base.navLeft) {
					base.$controls.find('.anythingNavWindow').animate(
						{ scrollLeft: base.navWidth(1, n), width: base.navWidth(n, n + o.navigationSize) },
						{ queue: false, duration: o.animationTime });
					base.navLeft = n;
				}
			}
		};

		// Creates the Forward/Backward buttons
		base.buildNextBackButtons = function() {
			base.$forward = $('<span class="arrow forward"><a href="#"><span>' + o.forwardText + '</span></a></span>');
			base.$back = $('<span class="arrow back"><a href="#"><span>' + o.backText + '</span></a></span>');

			// Bind to the forward and back buttons
			base.$back.bind(o.clickBackArrow, function(e) {
				// prevent running functions twice (once for click, second time for swipe)
				if (o.enableArrows && !base.flag) {
					base.flag = true; setTimeout(function(){ base.flag = false; }, 100);
					base.goBack();
				}
				e.preventDefault();
			});
			base.$forward.bind(o.clickForwardArrow, function(e) {
				// prevent running functions twice (once for click, second time for swipe)
				if (o.enableArrows && !base.flag) {
					base.flag = true; setTimeout(function(){ base.flag = false; }, 100);
					base.goForward();
				}
				e.preventDefault();
			});
			// using tab to get to arrow links will show they have focus (outline is disabled in css)
			base.$back.add(base.$forward).find('a').bind('focusin focusout',function(){
				$(this).toggleClass('hover');
			});

			// Append elements to page
			base.$back.appendTo( (o.appendBackTo && $(o.appendBackTo).length) ? $(o.appendBackTo) : base.$wrapper );
			base.$forward.appendTo( (o.appendForwardTo && $(o.appendForwardTo).length) ? $(o.appendForwardTo) : base.$wrapper );

			base.arrowWidth = base.$forward.width(); // assuming the left & right arrows are the same width - used for toggle
			base.arrowRight = parseInt(base.$forward.css('right'), 10);
			base.arrowLeft = parseInt(base.$back.css('left'), 10);

		};

		// Creates the Start/Stop button
		base.buildAutoPlay = function(){
			base.$startStop
				.html('<span>' + (base.playing ? o.stopText : o.startText) + '</span>')
				.bind(o.clickSlideshow, function(e) {
					if (o.enableStartStop) {
						base.startStop(!base.playing);
						base.makeActive();
						if (base.playing && !o.autoPlayDelayed) {
							base.goForward(true);
						}
					}
					e.preventDefault();
				})
				// show button has focus while tabbing
				.bind('focusin focusout',function(){
					$(this).toggleClass('hover');
				});
		};

		// Adjust slider dimensions on parent element resize
		base.checkResize = function(stopTimer){
			clearTimeout(base.resizeTimer);
			base.resizeTimer = setTimeout(function(){
				var w = base.$outer.width() - base.outerPad[0],
					h = (base.$outer[0].tagName === "BODY" ? base.$win.height() : base.$outer.height()) - base.outerPad[1];
				// base.width = width of one panel, so multiply by # of panels; outerPad is padding added for arrows.
				if (base.width * o.showMultiple !== w || base.height !== h) {
					base.setDimensions(); // adjust panel sizes
					// make sure page is lined up (use -1 animation time, so we can differeniate it from when animationTime = 0)
					base.gotoPage(base.currentPage, base.playing, null, -1);
				}
				if (typeof(stopTimer) === 'undefined'){ base.checkResize(); }
			}, 500);
		};

		// Set panel dimensions to either resize content or adjust panel to content
		base.setDimensions = function(){

			// reset element width & height
			base.$wrapper.find('.anythingWindow, .anythingBase, .panel').andSelf().css({ width: '', height: '' });
			base.width = base.$el.width();
			base.height = base.$el.height();
			base.outerPad = [ base.$wrapper.innerWidth() - base.$wrapper.width(), base.$wrapper.innerHeight() - base.$wrapper.height() ];

			var w, h, c, t, edge = 0,
				fullsize = { width: '100%', height: '100%' },
				// determine panel width
				pw = (o.showMultiple > 1) ? base.width || base.$window.width()/o.showMultiple : base.$window.width(),
				winw = base.$win.width();
			if (o.expand){
				w = base.$outer.css('height','').width() - base.outerPad[0];
				base.height = h = base.$outer.height() - base.outerPad[1];
				base.$wrapper.add(base.$window).add(base.$items).css({ width: w, height: h });
				base.width = pw = (o.showMultiple > 1) ? w/o.showMultiple : w;
			}
			base.$items.each(function(i){
				t = $(this);
				c = t.children();
				if (o.resizeContents){
					// resize panel
					w = base.width;
					h = base.height;
					t.css({ width: w, height: h });
					if (c.length) {
						if (c[0].tagName === "EMBED") { c.attr(fullsize); } // needed for IE7; also c.length > 1 in IE7
						if (c[0].tagName === "OBJECT") { c.find('embed').attr(fullsize); }
						// resize panel contents, if solitary (wrapped content or solitary image)
						if (c.length === 1){ c.css(fullsize); }
					}
				} else {
					// get panel width & height and save it
					w = t.width() || base.width; // if image hasn't finished loading, width will be zero, so set it to base width instead
					if (c.length === 1 && w >= winw){
						w = (c.width() >= winw) ? pw : c.width(); // get width of solitary child
						c.css('max-width', w);   // set max width for all children
					}
					t.css({ width: w, height: '' }); // set width of panel
					h = (c.length === 1 ? c.outerHeight(true) : t.height()); // get height after setting width
					if (h <= base.outerPad[1]) { h = base.height; } // if height less than the outside padding, then set it to the preset height
					t.css('height', h);
				}
				base.panelSize[i] = [w,h,edge];
				edge += (o.mode === 'vertical') ? h : w;
			});
			// Set total width of slider
			base.$el.css((o.mode === 'vertical' ? 'height' : 'width'), o.mode === 'fade' ? base.width : edge );
		};

		// get dimension of multiple panels, as needed
		base.getDim = function(page){
			var i, w = base.width, h = base.height;
			if (base.pages < 1 || isNaN(page)) { return [ w, h ]; } // prevent errors when base.panelSize is empty
			page = (o.infiniteSlides && base.pages > 1) ? page : page - 1;
			i = base.panelSize[page];
			if (i) {
				w = i[0] || w;
				h = i[1] || h;
			}
			if (o.showMultiple > 1) {
				for (i=1; i < o.showMultiple; i++) {
					w += base.panelSize[(page + i)][0];
					h = Math.max(h, base.panelSize[page + i][1]);
				}
			}
			return [w,h];
		};

		base.goForward = function(autoplay) {
			// targetPage changes before animation so if rapidly changing pages, it will have the correct current page
			base.gotoPage(base[ o.allowRapidChange ? 'targetPage' : 'currentPage'] + o.changeBy * (o.playRtl ? -1 : 1), autoplay);
		};

		base.goBack = function(autoplay) {
			base.gotoPage(base[ o.allowRapidChange ? 'targetPage' : 'currentPage'] + o.changeBy * (o.playRtl ? 1 : -1), autoplay);
		};

		base.gotoPage = function(page, autoplay, callback, time) {
			if (autoplay !== true) {
				autoplay = false;
				base.startStop(false);
				base.makeActive();
			}
			// check if page is an id or class name
			if (/^[#|.]/.test(page) && $(page).length) {
				page = $(page).closest('.panel').index() + base.adj;
			}

			// rewind effect occurs here when changeBy > 1
			if (o.changeBy !== 1){
				var adj = base.pages - base.adjustMultiple;
				if (page < 1) {
					page = o.stopAtEnd ? 1 : ( o.infiniteSlides ? base.pages + page : ( o.showMultiple > 1 - page ? 1 : adj ) );
				}
				if (page > base.pages) {
					// 
					page = o.stopAtEnd ? base.pages : ( o.showMultiple > 1 - page ? 1 : page -= adj );
				} else if (page >= adj) {
					// show multiple adjustments
					page = adj;
				}
			}

			if (base.pages <= 1) { return; } // prevents animation
			base.$lastPage = base.$currentPage;
			if (typeof(page) !== "number") {
				page = parseInt(page,10) || o.startPanel;
				base.setCurrentPage(page);
			}

			// pause YouTube videos before scrolling or prevent change if playing
			if (autoplay && o.isVideoPlaying(base)) { return; }

			base.exactPage = page;
			if (page > base.pages + 1 - base.adj) { page = (!o.infiniteSlides && !o.stopAtEnd) ? 1 : base.pages; }
			if (page < base.adj ) { page = (!o.infiniteSlides && !o.stopAtEnd) ? base.pages : 1; }
			if (!o.infiniteSlides) { base.exactPage = page; } // exact page used by the fx extension
			base.currentPage = ( page > base.pages ) ? base.pages : ( page < 1 ) ? 1 : base.currentPage;
			base.$currentPage = base.$items.eq(base.currentPage - base.adj);
			base.targetPage = (page === 0) ? base.pages : (page > base.pages) ? 1 : page;
			base.$targetPage = base.$items.eq(base.targetPage - base.adj);
			time = typeof time !== 'undefined' ? time : o.animationTime;
			// don't trigger events when time < 0 - to prevent FX from firing multiple times on page resize
			if (time >= 0) { base.$el.trigger('slide_init', base); }
			// toggle arrows/controls only if there is time to see it - fix issue #317
			if (time > 0) { base.slideControls(true); }

			// Set visual
			if (o.buildNavigation){
				base.setNavigation(base.targetPage);
			}

			// When autoplay isn't passed, we stop the timer
			if (autoplay !== true) { autoplay = false; }
			// Stop the slider when we reach the last page, if the option stopAtEnd is set to true
			if (!autoplay || (o.stopAtEnd && page === base.pages)) { base.startStop(false); }

			if (time >= 0) { base.$el.trigger('slide_begin', base); }

			// delay starting slide animation
			setTimeout(function(d){
				var p, empty = true;
				if (o.allowRapidChange) {
					base.$wrapper.add(base.$el).add(base.$items).stop(true, true);
				}
				// resize slider if content size varies
				if (!o.resizeContents) {
					// animating the wrapper resize before the window prevents flickering in Firefox
					// don't animate the dimension if it hasn't changed - fix for issue #264
					p = base.getDim(page); d = {};
					// prevent animating a dimension to zero
					if (base.$wrapper.width() !== p[0]) { d.width = p[0] || base.width; empty = false; }
					if (base.$wrapper.height() !== p[1]) { d.height = p[1] || base.height; empty = false; }
					if (!empty) {
						base.$wrapper.filter(':not(:animated)').animate(d, { queue: false, duration: (time < 0 ? 0 : time), easing: o.easing });
					}
				}

				if (o.mode === 'fade') {
					if (base.$lastPage[0] !== base.$targetPage[0]) {
						base.fadeIt( base.$lastPage, 0, time );
						base.fadeIt( base.$targetPage, 1, time, function(){ base.endAnimation(page, callback, time); });
					} else {
						base.endAnimation(page, callback, time);
					}
				} else {
					d = {};
					d[base.dir] = -base.panelSize[(o.infiniteSlides && base.pages > 1) ? page : page - 1][2];
					// Animate Slider
					base.$el.filter(':not(:animated)').animate(
						d, { queue: false, duration: time < 0 ? 0 : time, easing: o.easing, complete: function(){ base.endAnimation(page, callback, time); } }
					);
				}
			}, parseInt(o.delayBeforeAnimate, 10) || 0);
		};

		base.endAnimation = function(page, callback, time){
			if (page === 0) {
				base.$el.css( base.dir, o.mode === 'fade' ? 0 : -base.panelSize[base.pages][2]);
				page = base.pages;
			} else if (page > base.pages) {
				// reset back to start position
				base.$el.css( base.dir, o.mode === 'fade' ? 0 : -base.panelSize[1][2]);
				page = 1;
			}
			base.exactPage = page;
			base.setCurrentPage(page, false);

			if (o.mode === 'fade') {
				// make sure non current panels are hidden (rapid slide changes)
				base.fadeIt( base.$items.not(':eq(' + (page - base.adj) + ')'), 0, 0);
			}

			if (!base.hovered) { base.slideControls(false); }

			if (o.hashTags) { base.setHash(page); }

			if (time >= 0) { base.$el.trigger('slide_complete', base); }
			// callback from external slide control: $('#slider').anythingSlider(4, function(slider){ })
			if (typeof callback === 'function') { callback(base); }

			// Continue slideshow after a delay
			if (o.autoPlayLocked && !base.playing) {
				setTimeout(function(){
					base.startStop(true);
				// subtract out slide delay as the slideshow waits that additional time.
				}, o.resumeDelay - (o.autoPlayDelayed ? o.delay : 0));
			}
		};

		base.fadeIt = function(el, toOpacity, time, callback){
			var t = time < 0 ? 0 : time;
			if (o.resumeOnVisible) {
				el.filter(':not(:animated)').fadeTo(t, toOpacity, callback);
			} else {
				el.filter(':not(:animated)')[ toOpacity === 0 ? 'fadeOut' : 'fadeIn' ](t, callback);
			}
		};

		base.setCurrentPage = function(page, move) {
			page = parseInt(page, 10);

			if (base.pages < 1 || page === 0 || isNaN(page)) { return; }
			if (page > base.pages + 1 - base.adj) { page = base.pages - base.adj; }
			if (page < base.adj ) { page = 1; }

			// hide/show arrows based on infinite scroll mode
			if (o.buildArrows && !o.infiniteSlides && o.stopAtEnd){
				base.$forward[ page === base.pages - base.adjustMultiple ? 'addClass' : 'removeClass']('disabled');
				base.$back[ page === 1 ? 'addClass' : 'removeClass']('disabled');
				if (page === base.pages && base.playing) { base.startStop(); }
			}

			// Only change left if move does not equal false
			if (!move) {
				var d = base.getDim(page);
				base.$wrapper
					.css({ width: d[0], height: d[1] })
					.add(base.$window).scrollLeft(0).scrollTop(0); // reset in case tabbing changed this scrollLeft - probably overly redundant
				base.$el.css( base.dir, o.mode === 'fade' ? 0 : -base.panelSize[(o.infiniteSlides && base.pages > 1) ? page : page - 1][2] );
			}

			// Update local variable
			base.currentPage = page;
			base.$currentPage = base.$items.removeClass('activePage').eq(page - base.adj).addClass('activePage');

			if (o.buildNavigation){
				base.setNavigation(page);
			}

		};

		base.setNavigation = function(page){
			base.$nav
				.find('.cur').removeClass('cur').end()
				.find('a').eq(page - 1).addClass('cur');
		};

		base.makeActive = function(){
			// Set current slider as active so keyboard navigation works properly
			if (!base.$wrapper.hasClass('activeSlider')){
				$('.activeSlider').removeClass('activeSlider');
				base.$wrapper.addClass('activeSlider');
			}
		};

		// This method tries to find a hash that matches an ID and panel-X
		// If either found, it tries to find a matching item
		// If that is found as well, then it returns the page number
		base.gotoHash = function(){
			var h = base.win.location.hash,
				i = h.indexOf('&'),
				n = h.match(base.regex);
			// test for "/#/" or "/#!/" used by the jquery address plugin - $('#/') breaks jQuery
			if (n === null && !/^#&/.test(h) && !/#!?\//.test(h) && !/\=/.test(h)) {
				// #quote2&panel1-3&panel3-3
				h = h.substring(0, (i >= 0 ? i : h.length));
				// ensure the element is in the same slider
				n = ($(h).length && $(h).closest('.anythingBase')[0] === base.el) ? base.$items.index($(h).closest('.panel')) + base.adj : null;
			} else if (n !== null) {
				// #&panel1-3&panel3-3
				n = (o.hashTags) ? parseInt(n[1],10) : null;
			}
			return n;
		};

		base.setHash = function(n){
			var s = 'panel' + base.runTimes + '-',
				h = base.win.location.hash;
			if ( typeof h !== 'undefined' ) {
				base.win.location.hash = (h.indexOf(s) > 0) ? h.replace(base.regex, s + n) : h + "&" + s + n;
			}
		};

		// Slide controls (nav and play/stop button up or down)
		base.slideControls = function(toggle){
			var dir = (toggle) ? 'slideDown' : 'slideUp',
				t1 = (toggle) ? 0 : o.animationTime,
				t2 = (toggle) ? o.animationTime : 0,
				op = (toggle) ? 1 : 0,
				sign = (toggle) ? 0 : 1; // 0 = visible, 1 = hidden
			if (o.toggleControls) {
				base.$controls.stop(true,true).delay(t1)[dir](o.animationTime/2).delay(t2);
			}
			if (o.buildArrows && o.toggleArrows) {
				if (!base.hovered && base.playing) { sign = 1; op = 0; } // don't animate arrows during slideshow
				base.$forward.stop(true,true).delay(t1).animate({ right: base.arrowRight + (sign * base.arrowWidth), opacity: op }, o.animationTime/2);
				base.$back.stop(true,true).delay(t1).animate({ left: base.arrowLeft + (sign * base.arrowWidth), opacity: op }, o.animationTime/2);
			}
		};

		base.clearTimer = function(paused){
			// Clear the timer only if it is set
			if (base.timer) {
				base.win.clearInterval(base.timer);
				if (!paused && base.slideshow) {
					base.$el.trigger('slideshow_stop', base);
					base.slideshow = false;
				}
			}
		};

		// Pass startStop(false) to stop and startStop(true) to play
		base.startStop = function(playing, paused) {
			if (playing !== true) { playing = false; }  // Default if not supplied is false
			base.playing = playing;

			if (playing && !paused) {
				base.$el.trigger('slideshow_start', base);
				base.slideshow = true;
			}

			// Toggle playing and text
			if (o.buildStartStop) {
				base.$startStop.toggleClass('playing', playing).find('span').html( playing ? o.stopText : o.startText );
				// add button text to title attribute if it is hidden by text-indent
				if ( base.$startStop.find('span').css('visibility') === "hidden" ) {
					base.$startStop.addClass(o.tooltipClass).attr( 'title', playing ? o.stopText : o.startText );
				}
			}

			// Pause slideshow while video is playing
			if (playing){
				base.clearTimer(true); // Just in case this was triggered twice in a row
				base.timer = base.win.setInterval(function() {
					if ( !!(doc.hidden || doc.webkitHidden || doc.mozHidden || doc.msHidden) ) {
						// stop slideshow if the page isn't visible (issue #463)
						if (!o.autoPlayLocked) {
							base.startStop();
						}
					} else if ( !o.isVideoPlaying(base) ) {
						// prevent autoplay if video is playing
						base.goForward(true);
					} else if (!o.resumeOnVideoEnd) {
						// stop slideshow if resume if false
						base.startStop();
					}
				}, o.delay);
			} else {
				base.clearTimer();
			}
		};

		// Trigger the initialization
		base.init();
	};

	$.anythingSlider.defaults = {
		// Appearance
		theme               : "default", // Theme name, add the css stylesheet manually
		mode                : "horiz",   // Set mode to "horizontal", "vertical" or "fade" (only first letter needed); replaces vertical option
		expand              : false,     // If true, the entire slider will expand to fit the parent element
		resizeContents      : true,      // If true, solitary images/objects in the panel will expand to fit the viewport
		showMultiple        : false,     // Set this value to a number and it will show that many slides at once
		easing              : "swing",   // Anything other than "linear" or "swing" requires the easing plugin or jQuery UI

		buildArrows         : true,      // If true, builds the forwards and backwards buttons
		buildNavigation     : true,      // If true, builds a list of anchor links to link to each panel
		buildStartStop      : true,      // ** If true, builds the start/stop button

/*
		// commented out as this will reduce the size of the minified version
		appendForwardTo     : null,      // Append forward arrow to a HTML element (jQuery Object, selector or HTMLNode), if not null
		appendBackTo        : null,      // Append back arrow to a HTML element (jQuery Object, selector or HTMLNode), if not null
		appendControlsTo    : null,      // Append controls (navigation + start-stop) to a HTML element (jQuery Object, selector or HTMLNode), if not null
		appendNavigationTo  : null,      // Append navigation buttons to a HTML element (jQuery Object, selector or HTMLNode), if not null
		appendStartStopTo   : null,      // Append start-stop button to a HTML element (jQuery Object, selector or HTMLNode), if not null
*/

		toggleArrows        : false,     // If true, side navigation arrows will slide out on hovering & hide @ other times
		toggleControls      : false,     // if true, slide in controls (navigation + play/stop button) on hover and slide change, hide @ other times

		startText           : "Start",   // Start button text
		stopText            : "Stop",    // Stop button text
		forwardText         : "&raquo;", // Link text used to move the slider forward (hidden by CSS, replaced with arrow image)
		backText            : "&laquo;", // Link text used to move the slider back (hidden by CSS, replace with arrow image)
		tooltipClass        : "tooltip", // Class added to navigation & start/stop button (text copied to title if it is hidden by a negative text indent)

		// Function
		enableArrows        : true,      // if false, arrows will be visible, but not clickable.
		enableNavigation    : true,      // if false, navigation links will still be visible, but not clickable.
		enableStartStop     : true,      // if false, the play/stop button will still be visible, but not clickable. Previously "enablePlay"
		enableKeyboard      : true,      // if false, keyboard arrow keys will not work for this slider.

		// Navigation
		startPanel          : 1,         // This sets the initial panel
		changeBy            : 1,         // Amount to go forward or back when changing panels.
		hashTags            : true,      // Should links change the hashtag in the URL?
		infiniteSlides      : true,      // if false, the slider will not wrap & not clone any panels
		navigationFormatter : null,      // Details at the top of the file on this use (advanced use)
		navigationSize      : false,     // Set this to the maximum number of visible navigation tabs; false to disable

		// Slideshow options
		autoPlay            : false,     // If true, the slideshow will start running; replaces "startStopped" option
		autoPlayLocked      : false,     // If true, user changing slides will not stop the slideshow
		autoPlayDelayed     : false,     // If true, starting a slideshow will delay advancing slides; if false, the slider will immediately advance to the next slide when slideshow starts
		pauseOnHover        : true,      // If true & the slideshow is active, the slideshow will pause on hover
		stopAtEnd           : false,     // If true & the slideshow is active, the slideshow will stop on the last page. This also stops the rewind effect when infiniteSlides is false.
		playRtl             : false,     // If true, the slideshow will move right-to-left

		// Times
		delay               : 3000,      // How long between slideshow transitions in AutoPlay mode (in milliseconds)
		resumeDelay         : 15000,     // Resume slideshow after user interaction, only if autoplayLocked is true (in milliseconds).
		animationTime       : 600,       // How long the slideshow transition takes (in milliseconds)
		delayBeforeAnimate  : 0,         // How long to pause slide animation before going to the desired slide (used if you want your "out" FX to show).

/*
		// Callbacks - commented out to reduce size of the minified version - they still work
		onBeforeInitialize  : function(e, slider) {}, // Callback before the plugin initializes
		onInitialized       : function(e, slider) {}, // Callback when the plugin finished initializing
		onShowStart         : function(e, slider) {}, // Callback on slideshow start
		onShowStop          : function(e, slider) {}, // Callback after slideshow stops
		onShowPause         : function(e, slider) {}, // Callback when slideshow pauses
		onShowUnpause       : function(e, slider) {}, // Callback when slideshow unpauses - may not trigger properly if user clicks on any controls
		onSlideInit         : function(e, slider) {}, // Callback when slide initiates, before control animation
		onSlideBegin        : function(e, slider) {}, // Callback before slide animates
		onSlideComplete     : function(slider) {},    // Callback when slide completes - no event variable!
*/

		// Interactivity
		clickForwardArrow   : "click",         // Event used to activate forward arrow functionality (e.g. add jQuery mobile's "swiperight")
		clickBackArrow      : "click",         // Event used to activate back arrow functionality (e.g. add jQuery mobile's "swipeleft")
		clickControls       : "click focusin", // Events used to activate navigation control functionality
		clickSlideshow      : "click",         // Event used to activate slideshow play/stop button
		allowRapidChange    : false,           // If true, allow rapid changing of the active pane, instead of ignoring activity during animation

		// Video
		resumeOnVideoEnd    : true,      // If true & the slideshow is active & a supported video is playing, it will pause the autoplay until the video is complete
		resumeOnVisible     : true,      // If true the video will resume playing (if previously paused, except for YouTube iframe - known issue); if false, the video remains paused.
		addWmodeToObject    : "opaque",  // If your slider has an embedded object, the script will automatically add a wmode parameter with this setting
		isVideoPlaying      : function(base){ return false; } // return true if video is playing or false if not - used by video extension

	};

	$.fn.anythingSlider = function(options, callback) {

		return this.each(function(){
			var page, anySlide = $(this).data('AnythingSlider');

			// initialize the slider but prevent multiple initializations
			if ((typeof(options)).match('object|undefined')){
				if (!anySlide) {
					(new $.anythingSlider(this, options));
				} else {
					anySlide.updateSlider();
				}
			// If options is a number, process as an external link to page #: $(element).anythingSlider(#)
			} else if (/\d/.test(options) && !isNaN(options) && anySlide) {
				page = (typeof(options) === "number") ? options : parseInt($.trim(options),10); // accepts "  2  "
				// ignore out of bound pages
				if ( page >= 1 && page <= anySlide.pages ) {
					anySlide.gotoPage(page, false, callback); // page #, autoplay, one time callback
				}
			// Accept id or class name
			} else if (/^[#|.]/.test(options) && $(options).length) {
				anySlide.gotoPage(options, false, callback);
			}
		});
	};

})(jQuery, window, document);
﻿/*
 * AnythingSlider Video Controller 1.3 beta for AnythingSlider v1.6+
 * By Rob Garrison (aka Mottie & Fudgey)
 * Dual licensed under the MIT and GPL licenses.
 */
(function($) {
	$.fn.anythingSliderVideo = function(options){

		//Set the default values, use comma to separate the settings, example:
		var defaults = {
			videoID : 'asvideo' // id prefix
		};

		return this.each(function(){
			// make sure a AnythingSlider is attached
			var video, tmp, service, sel, base = $(this).data('AnythingSlider');
			if (!base) { return; }
			video = base.video = {};
			// Next update, I may just force users to call the video extension instead of it auto-running on window load
			// then they can change the video options in that call instead of the base defaults, and maybe prevent the
			// videos being initialized twice on startup (once as a regular video and second time with the API string)
			video.options = $.extend({}, defaults, options);

			// check if SWFObject is loaded
			video.hasSwfo = (typeof(swfobject) !== 'undefined' && swfobject.hasOwnProperty('embedSWF') && typeof(swfobject.embedSWF) === 'function') ? true : false;

			video.list = {};
			video.hasVid = false;
			video.hasEmbed = false;
			video.services = $.fn.anythingSliderVideo.services;
			video.len = 0; // used to add a unique ID to videos "asvideo#"
			video.hasEmbedCount = 0;
			video.hasiframeCount = 0;
			video.$items = base.$items.filter(':not(.cloned)');

			// find and save all known videos
			for (service in video.services) {
				if (typeof(service) === 'string') {
					sel = video.services[service].selector;
					video.$items.find(sel).each(function(){
						tmp = $(this);
						// save panel and video selector in the list
						tmp.attr('id', video.options.videoID + video.len);
						video.list[video.len] = {
							id       : video.options.videoID + video.len++,
							panel    : tmp.closest('.panel')[0],
							service  : service,
							selector : sel,
							status   : -1 // YouTube uses -1 to mean the video is unstarted 
						};
						video.hasVid = true;
						if (sel.match('embed|object')) {
							video.hasEmbed = true;
							video.hasEmbedCount++;
						} else if (sel.match('iframe')) {
							video.hasiframeCount++;
						}
					});
				}
			}

			// Initialize each video, as needed
			$.each(video.list, function(i,s){
				// s.id = ID, s.panel = slider panel (DOM), s.selector = 'jQuery selector'
				var tmp, $tar, vidsrc, opts,
					$vid = $(s.panel).find(s.selector),
					service = video.services[s.service],
					api = service.initAPI || '';
				// Initialize embeded video javascript api using SWFObject, if loaded
				if (video.hasEmbed && video.hasSwfo && s.selector.match('embed|object')) {
					$vid.each(function(){
						// Older IE doesn't have an object - just make sure we are wrapping the correct element
						$tar = ($(this).parent()[0].tagName === 'OBJECT') ? $(this).parent() : $(this);
						vidsrc = ($tar[0].tagName === 'EMBED') ? $tar.attr('src') : $tar.find('embed').attr('src') || $tar.children().filter('[name=movie]').attr('value');
						opts = $.extend(true, {}, {
							flashvars : null,
							params    : { allowScriptAccess: 'always', wmode : base.options.addWmodeToObject, allowfullscreen : true },
							attr      : { 'class' : $tar.attr('class'), 'style' : $tar.attr('style'), 'data-url' : vidsrc }
						}, service.embedOpts);
						$tar.wrap('<div id="' + s.id + '"></div>');
						// use SWFObject if it exists, it replaces the wrapper with the object/embed
						swfobject.embedSWF(vidsrc + (api === '' ? '': api + s.id), s.id,
							$tar.attr('width'), $tar.attr('height'), '10', null,
							opts.flashvars, opts.params, opts.attr, function(){
								// run init code if it exists
								if (service.hasOwnProperty('init')) {
									video.list[i].player = service.init(base, s.id, i);
								}
								if (i >= video.hasEmbedCount) {
									base.$el.trigger('swf_completed', base); // swf callback
								}
							}
						);
					});
				} else if (s.selector.match('iframe')) {
					$vid.each(function(i,v){
						vidsrc = $(this).attr('src');
						tmp = (vidsrc.match(/\?/g) ? '' : '?') + '&wmode=' + base.options.addWmodeToObject; // string connector & wmode
						$(this).attr('src', function(i,r){ return r + tmp + (api === '' ? '': api + s.id); });
					});
				}
			});

			// Returns URL parameter; url: http://www.somesite.com?name=hello&id=11111
			// Original code from Netlobo.com (http://www.netlobo.com/url_query_string_javascript.html)
			video.gup = function(n,s){
				n = n.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
				var p = (new RegExp("[\\?&]"+n+"=([^&#]*)")).exec(s || window.location.href);
				return (p===null) ? "" : p[1];
			};

			// postMessage to iframe - http://benalman.com/projects/jquery-postmessage-plugin/ (FOR IE7)
			video.postMsg = function(data, vid){
				var $vid = $('#' + vid);
				if ($vid.length){
					$vid[0].contentWindow.postMessage(data, $vid.attr('src').split('?')[0]);
				}
			};

			// receive message from iframe
			// no way to figure out which iframe since the message is from the window
			video.message = function(e){
				if (e.data) {
					if (/infoDelivery/g.test(e.data)) { return; } // ignore youtube video loading spam
					var data = $.parseJSON(e.data);
					$.each(video.list, function(i,s){
						if (video.services[video.list[i].service].hasOwnProperty('message')) {
							video.services[video.list[i].service].message(base, data);
						}
					});
				}
			};

			// toDO = 'cont', 'pause' or 'isPlaying'
			video.control = function(toDo){
				var i,
					s = video.list,
					slide = (toDo === 'pause') ? base.$lastPage[0] : base.$currentPage[0],
					isPlaying = false;
				for (i=0; i < video.len; i++){
					if (s[i].panel === slide && video.services[s[i].service].hasOwnProperty(toDo)){
						isPlaying = video.services[s[i].service][toDo](base, s[i].id, i);
					}
				}
				return isPlaying;
			};

			// iframe event listener
			if (video.hasiframeCount){
				if (window.addEventListener){
					window.addEventListener('message', video.message, false);
				} else { // IE
					window.attachEvent('onmessage', video.message, false);
				}
			}

			// bind to events
			base.$el
				.bind('slide_init', function(){
					video.control('pause');
				})
				.bind('slide_complete', function(){
					video.control('cont');
				});

			base.options.isVideoPlaying = function(){ return video.control('isPlaying'); };

		});
	};

/* Each video service is set up as follows
 * service-name : {
 *  // initialization
 *  selector  : 'object[data-url*=service], embed[src*=service]', // required: jQuery selector used to find the video ('video' or 'iframe[src*=service]' are other examples)
 *  initAPI   : 'string added to the URL to initialize the API',  // optional: the string must end with a parameter pointing to the video id (e.g. "&player_id=")
 *  embedOpts : { flashvars: {}, params: {}, attr: {} },          // optional: add any required flashvars, parameters or attributes to initialize the API
 *  // video startup functions
 *  init      : function(base, vid, index){ }, // optional: include any additional initialization code here; function called AFTER the embeded video is added using SWFObject
 *  // required functions
 *  cont      : function(base, vid, index){ }, // required: continue play if video was previously played
 *  pause     : function(base, vid, index){ }, // required: pause ALL videos
 *  message   : function(base, data){ },       // required for iframe: process data received from iframe and update the video status for the "isPlaying" function
 *  isPlaying : function(base, vid, index){ }  // required: return true if video is playing and return false if not playing (paused or ended)
 * }
 *
 * Function variables
 *  base (object) = plugin base, all video values/functions are stored in base.video
 *  vid (string) is the ID of the video: vid = "asvideo1"; so jQuery needs a "#" in front... "#" + videoID option default ("asvideo") + index (e.g. "1"); each video matching a service will have a unquie vid
 *  index (number) is the unique video number from the vid (starts from zero)
 *
 *  var list = base.video.list[index]; list will contain:
 *   list.id = vid
 *   list.service = service name (e.g. 'video', 'vimeo1', 'vimeo2', etc)
 *   list.selector = 'jQuery selector' (e.g. 'video', 'object[data-url*=vimeo]', 'iframe[src*=vimeo]', etc)
 *   list.panel = AnythingSlider panel DOM object. So you can target the video using $(list[index].panel).find(list[index].service) or $('#' + vid)
 *   list.status = video status, updated by the iframe event listeners added in the video service "ready" function; see examples below
 */

$.fn.anythingSliderVideo.services = {

	// *** HTML5 video ***
	video : {
		selector : 'video',
		cont : function(base, vid, index){
			var $vid = $('#' + vid);
			if ($vid.length && $vid[0].paused && $vid[0].currentTime > 0 && !$vid[0].ended) {
				$vid[0].play();
			}
		},
		pause : function(base, vid){
			// pause ALL videos on the page
			$('video').each(function(){
				if (typeof(this.pause) !== 'undefined') { this.pause(); } // throws an error in older ie without this
			});
		},
		isPlaying : function(base, vid, index){
			var $vid = $('#' + vid);
			// media.paused seems to be the only way to determine if a video is playing
			return ($vid.length && typeof($vid[0].pause) !== 'undefined' && !$vid[0].paused && !$vid[0].ended) ? true : false;
		}
	},

	// *** Vimeo iframe *** isolated demo: http://jsfiddle.net/Mottie/GxwEX/
	vimeo1 : {
		selector : 'iframe[src*=vimeo]',
		initAPI : '&api=1&player_id=', // video ID added to the end
		cont : function(base, vid, index){
			if (base.options.resumeOnVisible && base.video.list[index].status === 'pause'){
				// Commands sent to the iframe originally had "JSON.stringify" applied to them,
				// but not all browsers support this, so it's just as easy to wrap it in quotes.
				base.video.postMsg('{"method":"play"}', vid);
			}
		},
		pause : function(base, vid){
			// pause ALL videos on the page
			$('iframe[src*=vimeo]').each(function(){
				base.video.postMsg('{"method":"pause"}', this.id);
			});
		},
		message : function(base, data){
			// *** VIMEO *** iframe uses data.player_id
			var index, vid = data.player_id || ''; // vid = data.player_id (unique to vimeo)
			if (vid !== ''){
				index = vid.replace(base.video.options.videoID, '');
				if (data.event === 'ready') {
					// Vimeo ready, add additional event listeners for video status
					base.video.postMsg('{"method":"addEventListener","value":"play"}', vid);
					base.video.postMsg('{"method":"addEventListener","value":"pause"}', vid);
					base.video.postMsg('{"method":"addEventListener","value":"finish"}', vid);
				}
				// update current status - vimeo puts it in data.event
				if (base.video.list[index]) { base.video.list[index].status = data.event; }
			}
		},
		isPlaying : function(base, vid, index){
			return (base.video.list[index].status === 'play') ? true : false;
		}
	},

	// *** Embeded Vimeo ***
	// SWFObject adds the url to the object data
	// using param as a selector, the script above looks for the parent if it sees "param"
	vimeo2 : {
		selector : 'object[data-url*=vimeo], embed[src*=vimeo]',
		embedOpts : { flashvars : { api : 1 } },
		cont : function(base, vid, index) {
			if (base.options.resumeOnVisible) {
				var $vid = $('#' + vid);
				// continue video if previously played & not finished (api_finish doesn't seem to exist) - duration can be a decimal number, so subtract it and look at the difference (2 seconds here)
				if (typeof($vid[0].api_play) === 'function' && $vid[0].api_paused() && $vid[0].api_getCurrentTime() !== 0 && ($vid[0].api_getDuration() - $vid[0].api_getCurrentTime()) > 2) {
					$vid[0].api_play();
				}
			}
		},
		pause : function(base, vid){
			// find ALL videos and pause them, just in case
			$('object[data-url*=vimeo], embed[src*=vimeo]').each(function(){
				var el = (this.tagName === 'EMBED') ? $(this).parent()[0] : this;
				if (typeof(el.api_pause) === 'function') {
					el.api_pause();
				}
			});
		},
		isPlaying : function(base, vid, index){
			var $vid = $('#' + vid);
			return (typeof($vid[0].api_paused) === 'function' && !$vid[0].api_paused()) ? true : false;
		}
	},

	// *** iframe YouTube *** isolated demo: http://jsfiddle.net/Mottie/qk5MY/
	youtube1 : {
		selector : 'iframe[src*=youtube]',
		// "iv_load_policy=3" should turn off annotations on init, but doesn't seem to
		initAPI : '&iv_load_policy=3&enablejsapi=1&playerapiid=',
		cont : function(base, vid, index){
			if (base.options.resumeOnVisible && base.video.list[index].status === 2){
				base.video.postMsg('{"event":"command","func":"playVideo"}', vid);
			}
		},
		pause : function(base, vid, index){
			// pause ALL videos on the page - in IE, pausing a video means it will continue when next seen =(
			$('iframe[src*=youtube]').each(function(){
//			if (this.id !== vid || (this.id === vid && base.video.list[index].status >= 0)) { // trying to fix the continue video problem; this only breaks it
				base.video.postMsg('{"event":"command","func":"pauseVideo"}', vid);
//			}
			});
		},
		message : function(base, data){
			if (data.event === 'infoDelivery') { return; } // ignore youtube video loading spam
			// *** YouTube *** iframe returns an embeded url (data.info.videoUrl) but no video id...
			if (data.info && data.info.videoUrl) {
				// figure out vid for youtube
				// data.info.videoURL = http://www.youtube.com/watch?v=###########&feature=player_embedded
				var url = base.video.gup('v', data.info.videoUrl), // end up with ###########, now find it
					v = $('iframe[src*=' + url + ']'), vid, index;
					// iframe src changes when watching related videos; so there is no way to tell which video has an update
					if (v.length) {
						vid = v[0].id;
						index = vid.replace(base.video.options.videoID, '');
					// YouTube ready, add additional event listeners for video status. BUT this never fires off =(
					// Fixing this may solve the continue problem
					if (data.event === 'onReady') {
						base.video.postMsg('{"event":"listening","func":"onStateChange"}', vid);
					}
					// Update status, so the "isPlaying" function can access it
					if (data.event === 'onStateChange' && base.video.list[index]) {
						// update list with current status; data.info.playerState = YouTube
						base.video.list[index].status = data.info.playerState;
					}
				}
			}
		},
		isPlaying : function(base, vid, index){
			var status = base.video.list[index].status;
			// state: unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
			return (status === 1 || status > 2) ? true : false;
		}
	},

	// *** Embeded YouTube ***
	// include embed for IE; SWFObject adds the url to the object data attribute
	youtube2 : {
		selector : 'object[data-url*=youtube], embed[src*=youtube]',
		initAPI : '&iv_load_policy=3&enablejsapi=1&version=3&playerapiid=', // video ID added to the end
		// YouTube - player states: unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
		cont : function(base, vid, index) {
			if (base.options.resumeOnVisible) {
				var $vid = $('#' + vid);
				// continue video if previously played and not cued
				if ($vid.length && typeof($vid[0].getPlayerState) === 'function' && $vid[0].getPlayerState() > 0) {
					$vid[0].playVideo();
				}
			}
		},
		pause : function(base, vid){
			// find ALL videos and pause them, just in case
			$('object[data-url*=youtube], embed[src*=youtube]').each(function(){
				var el = (this.tagName === 'EMBED') ? $(this).parent()[0] : this;
				// player states: unstarted (-1), ended (0), playing (1), paused (2), buffering (3), video cued (5).
				if (typeof(el.getPlayerState) === 'function' && el.getPlayerState() > 0) {
					// pause video if not autoplaying (if already initialized)
					el.pauseVideo();
				}
			});
		},
		isPlaying : function(base, vid){
			var $vid = $('#' + vid);
			return (typeof($vid[0].getPlayerState) === 'function' && ($vid[0].getPlayerState() === 1 || $vid[0].getPlayerState() > 2)) ? true : false;
		}
	}

};

})(jQuery);

// Initialize video extension automatically
jQuery(window).load(function(){
 jQuery('.anythingBase').anythingSliderVideo();
});

/*!
 * jQuery Video Background plugin
 * https://github.com/georgepaterson/jquery-videobackground
 *
 * Copyright 2012, George Paterson
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 */
(function ($, document, window) {
	/*
	 * Resize function.
	 * Triggered if the boolean setting 'resize' is true.
	 * It will resize the video background based on a resize event initiated on the browser window.
	 *
	 */
	"use strict";
	function resize(that) {
		var documentHeight = $(document).height(),
			windowHeight = $(window).height();
		if (that.settings.resizeTo === 'window') {
			$(that).css('height', windowHeight);
		} else {
			if (windowHeight >= documentHeight) {
				$(that).css('height', windowHeight);
			} else {
				$(that).css('height', documentHeight);
			}
		}
	}
	/*
	 * Preload function.
	 * Allows for HTML and JavaScript designated in settings to be used while the video is preloading.
	 *
	 */
	function preload(that) {
		$(that.controlbox).append(that.settings.preloadHtml);
		if (that.settings.preloadCallback) {
			(that.settings.preloadCallback).call(that);
		}
	}
	/*
	 * Play function.
	 * Can either be called through the default control interface or directly through the public method.
	 * Will set the video to play or pause depending on existing state.
	 * Requires the video to be loaded.
	 *
	 */
	function play(that) {
		var video = that.find('video').get(0),
			controller;
		if (that.settings.controlPosition) {
			controller = $(that.settings.controlPosition).find('.ui-video-background-play a');
		} else {
			controller = that.find('.ui-video-background-play a');
		}
		if (video.paused) {
			video.play();
			controller.toggleClass('ui-icon-pause ui-icon-play').text(that.settings.controlText[1]);
		} else {
			if (video.ended) {
				video.play();
				controller.toggleClass('ui-icon-pause ui-icon-play').text(that.settings.controlText[1]);
			} else {
				video.pause();
				controller.toggleClass('ui-icon-pause ui-icon-play').text(that.settings.controlText[0]);
			}
		}
	}
	/*
	 * Mute function.
	 * Can either be called through the default control interface or directly through the public method.
	 * Will set the video to mute or unmute depending on existing state.
	 * Requires the video to be loaded.
	 *
	 */
	function mute(that) {
		var video = that.find('video').get(0),
			controller;
		if (that.settings.controlPosition) {
			controller = $(that.settings.controlPosition).find('.ui-video-background-mute a');
		} else {
			controller = that.find('.ui-video-background-mute a');
		}
		if (video.volume === 0) {
			video.volume = 1;
			controller.toggleClass('ui-icon-volume-on ui-icon-volume-off').text(that.settings.controlText[2]);
		} else {
			video.volume = 0;
			controller.toggleClass('ui-icon-volume-on ui-icon-volume-off').text(that.settings.controlText[3]);
		}
	}
	/*
	 * Loaded events function.
	 * When the video is loaded we have some default HTML and JavaScript to trigger.
	 *
	 */
	function loadedEvents(that) {
		/*
		 * Trigger the resize method based if the browser is resized.
		 *
		 */
		if (that.settings.resize) {
			$(window).on('resize', function () {
				resize(that);
			});
		}
		/*
		 * Default play/pause control
		 *
		 */
		that.controls.find('.ui-video-background-play a').on('click', function (event) {
			event.preventDefault();
			play(that);
		});
		/*
		 * Default mute/unmute control
		 *
		 */
		that.controls.find('.ui-video-background-mute a').on('click', function (event) {
			event.preventDefault();
			mute(that);
		});
		/*
		 * Firefox doesn't currently use the loop attribute.
		 * Loop bound to the video ended event.
		 *
		 */
		if (that.settings.loop) {
			that.find('video').on('ended', function () {
				$(this).get(0).play();
				$(this).toggleClass('paused').text(that.settings.controlText[1]);
			});
		}
	}
	/*
	 * Loaded function.
	 * Allows for HTML and JavaScript designated in settings to be used when the video is loaded.
	 *
	 */
	function loaded(that) {
		$(that.controlbox).html(that.controls);
		loadedEvents(that);
		if (that.settings.loadedCallback) {
			(that.settings.loadedCallback).call(that);
		}
	}
	/*
	 * Public methods accessible through a string declaration equal to the method name.
	 *
	 */
	var methods = {
		/*
		 * Default initiating public method.
		 * It will created the video background, default video controls and initiate associated events.
		 *
		 */
		init: function (options) {
			return this.each(function () {
				var that = $(this),
					compiledSource = '',
					attributes = '',
					data = that.data('video-options'),
					image,
					isArray;
				if (document.createElement('video').canPlayType) {
					that.settings = $.extend(true, {}, $.fn.videobackground.defaults, data, options);
					if (!that.settings.initialised) {
						that.settings.initialised = true;
						/*
						 * If the resize option is set.
						 * Set the height of the container to be the height of the document
						 * The video can expand in to the space using min-height: 100%;
						 *
						 */
						if (that.settings.resize) {
							resize(that);
						}
						/*
						 * Compile the different HTML5 video attributes.
						 *
						 */
						$.each(that.settings.videoSource, function () {
							isArray = Object.prototype.toString.call(this) === '[object Array]';
							if (isArray && this[1] !== undefined) {
								compiledSource = compiledSource + '<source src="' + this[0] + '" type="' + this[1] + '">';
							} else {
								if (isArray) {
									compiledSource = compiledSource + '<source src="' + this[0] + '">';
								} else {
									compiledSource = compiledSource + '<source src="' + this + '">';
								}
							}
						});
						attributes = attributes + 'preload="' + that.settings.preload + '"';
						if (that.settings.poster) {
							attributes = attributes + ' poster="' + that.settings.poster + '"';
						}
						if (that.settings.autoplay) {
							attributes = attributes + ' autoplay="autoplay"';
						}
						if (that.settings.loop) {
							attributes = attributes + ' loop="loop"';
						}
						$(that).html('<video ' + attributes + '>' + compiledSource + '</video>');
						/*
						 * Append the control box either to the supplied that or the video background that.
						 *
						 */
						that.controlbox = $('<div class="ui-video-background ui-widget ui-widget-content ui-corner-all"></div>');
						if (that.settings.controlPosition) {
							$(that.settings.controlPosition).append(that.controlbox);
						} else {
							$(that).append(that.controlbox);
						}
						/*
						 *	HTML string for the video controls.
						 *
						 */
						that.controls = $('<ul class="ui-video-background-controls"><li class="ui-video-background-play">'
							+ '<a class="ui-icon ui-icon-pause" href="#">' + that.settings.controlText[1] + '</a>'
							+ '</li><li class="ui-video-background-mute">'
							+ '<a class="ui-icon ui-icon-volume-on" href="#">' + that.settings.controlText[2] + '</a>'
							+ '</li></ul>');
						/*
						 * Test for HTML or JavaScript function that should be triggered while the video is attempting to load.
						 * The canplaythrough event signals when when the video can play through to the end without disruption.
						 * We use this to determine when the video is ready to play.
						 * When this happens preloaded HTML and JavaSCript should be replaced with loaded HTML and JavaSCript.
						 *
						 */
						if (that.settings.preloadHtml || that.settings.preloadCallback) {
							preload(that);
							that.find('video').on('canplaythrough', function () {
								/*
								 * Chrome doesn't currently using the autoplay attribute.
								 * Autoplay initiated through JavaScript.
								 *
								 */
								if (that.settings.autoplay) {
									that.find('video').get(0).play();
								}
								loaded(that);
							});
						} else {
							that.find('video').on('canplaythrough', function () {
								/*
								 * Chrome doesn't currently using the autoplay attribute.
								 * Autoplay initiated through JavaScript.
								 *
								 */
								if (that.settings.autoplay) {
									that.find('video').get(0).play();
								}
								loaded(that);
							});
						}
						that.data('video-options', that.settings);
					}
				} else {
					that.settings = $.extend(true, {}, $.fn.videobackground.defaults, data, options);
					if (!that.settings.initialised) {
						that.settings.initialised = true;
						if (that.settings.poster) {
							image = $('<img class="ui-video-background-poster" src="' + that.settings.poster + '">');
							that.append(image);
						}
						that.data('video-options', that.settings);
					}
				}
			});
		},
		/*
		 * Play public method.
		 * When attached to a video background it will trigger the associated video to play or pause.
		 * The event it triggeres is dependant on the existing state of the video.
		 * This method can be triggered from an event on a external that.
		 * If the that has a unique controlPosition this will have to be declared.
		 * Requires the video to be loaded first.
		 *
		 */
		play: function (options) {
			return this.each(function () {
				var that = $(this),
					data = that.data('video-options');
				that.settings = $.extend(true, {}, data, options);
				if (that.settings.initialised) {
					play(that);
					that.data('video-options', that.settings);
				}
			});
		},
		/*
		 * Mute public method.
		 * When attached to a video background it will trigger the associated video to mute or unmute.
		 * The event it triggeres is dependant on the existing state of the video.
		 * This method can be triggered from an event on a external that.
		 * If the that has a unique controlPosition this will have to be declared.
		 * Requires the video to be loaded first.
		 *
		 */
		mute: function (options) {
			return this.each(function () {
				var that = $(this),
					data = that.data('video-options');
				that.settings = $.extend(true, {}, data, options);
				if (that.settings.initialised) {
					mute(that);
					that.data('video-options', that.settings);
				}
			});
		},
		/*
		 * Resize public method.
		 * When invoked will resize the video background to the height of the document or window.
		 * The video background height affects the height of the document.
		 * Affecting the video background's ability to negatively resize.
		 *
		 */
		resize: function (options) {
			return this.each(function () {
				var that = $(this),
					data = that.data('video-options');
				that.settings = $.extend(true, {}, data, options);
				if (that.settings.initialised) {
					resize(that);
					that.data('video-options', that.settings);
				}
			});
		},
		/*
		 * Destroy public method.
		 * Will unbind event listeners and remove HTML created by the plugin.
		 * If the that has a unique controlPosition this will have to be declared.
		 *
		 */
		destroy: function (options) {
			return this.each(function () {
				var that = $(this),
					data = that.data('video-options');
				that.settings = $.extend(true, {}, data, options);
				if (that.settings.initialised) {
					that.settings.initialised = false;
					if (document.createElement('video').canPlayType) {
						that.find('video').off('ended');
						if (that.settings.controlPosition) {
							$(that.settings.controlPosition).find('.ui-video-background-mute a').off('click');
							$(that.settings.controlPosition).find('.ui-video-background-play a').off('click');
						} else {
							that.find('.ui-video-background-mute a').off('click');
							that.find('.ui-video-background-play a').off('click');
						}
						$(window).off('resize');
						that.find('video').off('canplaythrough');
						if (that.settings.controlPosition) {
							$(that.settings.controlPosition).find('.ui-video-background').remove();
						} else {
							that.find('.ui-video-background').remove();
						}
						$('video', that).remove();
					} else {
						if (that.settings.poster) {
							that.find('.ui-video-background-poster').remove();
						}
					}
					that.removeData('video-options');
				}
			});
		}
	};
	/*
	 * The video background namespace.
	 * The gate way for the plugin.
	 *
	 */
	$.fn.videobackground = function (method) {
		/*
		 * Allow for method calling.
		 * If not a method initialise the plugin.
		 *
		 */
		if (!this.length) {
			return this;
		}
	    if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
	    }
		if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
	    }
		$.error('Method ' +  method + ' does not exist on jQuery.videobackground');
	};
	/*
	 * Default options, can be extend by options passed to the function.
	 *
	 */
	$.fn.videobackground.defaults = {
		videoSource: [],
		poster: null,
		autoplay: true,
		preload: 'none',
		loop: false,
		controlPosition: null,
		controlText: ['Play', 'Pause', 'Mute', 'Unmute'],
		resize: true,
		preloadHtml: '',
		preloadCallback: null,
		loadedCallback: null,
		resizeTo: 'document'
	};
}(jQuery, document, window));
/*
 * nyroModal v2.0.0
 * Core
 *
 * Commit 1ff104065fb155520ad2c6e52ec1753473ae7bf0 (10/21/2012) * 
 * 
 * Included parts:
 * - anims.fade
 * - filters.title
 * - filters.gallery
 * - filters.link
 * - filters.dom
 * - filters.data
 * - filters.image
 * - filters.swf
 * - filters.iframe
 * - filters.iframeForm
 * - filters.embedly
 */
/*
 * nyroModal v2.0.0
 * Core
 *
 */
jQuery(function($, undefined) {

	var $w = $(window),
		$d = $(document),
		$b = $('body'),
		baseHref = $('base').attr('href'),
		// nyroModal Object
		_nmObj = {
			filters: [],	// List of filters used
			callbacks: {},	// Sepcific callbacks
			anims: {},	// Sepcific animations functions
			loadFilter: undefined,	// Name of the filter used for loading

			modal: false,	// Indicates if it's a modal window or not
			closeOnEscape: true,	// Indicates if the modal should close on Escape key
			closeOnClick: true,	// Indicates if a click on the background should close the modal
			useKeyHandler: false,	// Indicates if the modal has to handle key down event

			showCloseButton: true,	// Indicates if the closeButonn should be added
			closeButton: '<a href="#" class="nyroModalClose nyroModalCloseButton nmReposition" title="close">Close</a>',	// Close button HTML

			stack: false,	// Indicates if links automatically binded inside the modal should stack or not
			nonStackable: 'form',	// Filter to not stack DOM element

			header: undefined,	// header include in every modal
			footer: undefined,	// footer include in every modal
			
			// Specific confirguation for gallery filter
			galleryLoop: true,	// Indicates if the gallery should loop
			galleryCounts: true,	// Indicates if the gallery counts should be shown
			ltr: true, // Left to Right by default. Put to false for Hebrew or Right to Left language. Used in gallery filter

			// Specific confirguation for DOM filter
			domCopy: false, // Indicates if DOM element should be copied or moved
			
			// Specific confirguation for link and form filters
			ajax: {}, // Ajax options to be used in link and form filter
			
			// Specific confirguation for image filter
			imageRegex: '[^\.]\.(jpg|jpeg|png|tiff|gif|bmp)\s*$',	// Regex used to detect image link

			selIndicator: 'nyroModalSel', // Value added when a form or Ajax is sent with a filter content

			swfObjectId: undefined, // Object id for swf object
			swf:  {	// Default SWF attributes
				allowFullScreen: 'true',
				allowscriptaccess: 'always',
				wmode: 'transparent'
			},

			store: {},	// Storage object for filters.
			errorMsg: 'An error occured',	// Error message
			elts: {	// HTML elements for the modal
				all: undefined,
				bg: undefined,
				load: undefined,
				cont: undefined,
				hidden: undefined
			},
			sizes: {	// Size information
				initW: undefined,	// Initial width
				initH: undefined,	// Initial height
				w: undefined,		// width
				h: undefined,		// height
				minW: undefined,	// minimum Width
				minH: undefined,	// minimum height
				wMargin: undefined,	// Horizontal margin
				hMargin: undefined	// Vertical margin
			},
			anim: {	// Animation names to use
				def: undefined,			// Default animation set to use if sspecific are not defined or doesn't exist
				showBg: undefined,		// Set to use for showBg animation
				hideBg: undefined,		// Set to use for hideBg animation
				showLoad: undefined,	// Set to use for showLoad animation
				hideLoad: undefined,	// Set to use for hideLoad animation
				showCont: undefined,	// Set to use for showCont animation
				hideCont: undefined,	// Set to use for hideCont animation
				showTrans: undefined,	// Set to use for showTrans animation
				hideTrans: undefined,	// Set to use for hideTrans animation
				resize: undefined		// Set to use for resize animation
			},

			_open: false,	// Indicates if the modal is open
			_bgReady: false,	// Indicates if the background is ready
			_opened: false,	// Indicates if the modal was opened (useful for stacking)
			_loading: false,	// Indicates if the loading is shown
			_animated: false,	// Indicates if the modal is currently animated
			_transition: false,	//Indicates if the modal is in transition
			_nmOpener: undefined,	// nmObj of the modal that opened the current one in non stacking mode
			_nbContentLoading: 0,	// Counter for contentLoading call
			_scripts: '',	// Scripts tags to be included
			_scriptsShown: '',	//Scripts tags to be included once the modal is swhon

			// save the object in data
			saveObj: function() {
				this.opener.data('nmObj', this);
			},
			// Open the modal
			open: function() {
				if (this._nmOpener)
					this._nmOpener._close();
				this.getInternal()._pushStack(this.opener);
				this._opened = false;
				this._bgReady = false;
				this._open = true;
				this._initElts();
				this._load();
				this._nbContentLoading = 0;
				this._callAnim('showBg', $.proxy(function() {
					this._bgReady = true;
					if (this._nmOpener) {
						// fake closing of the opener nyroModal
						this._nmOpener._bgReady = false;
						this._nmOpener._loading = false;
						this._nmOpener._animated = false;
						this._nmOpener._opened = false;
						this._nmOpener._open = false;
						this._nmOpener.elts.cont = this._nmOpener.elts.hidden = this._nmOpener.elts.load = this._nmOpener.elts.bg = this._nmOpener.elts.all = undefined;
						this._nmOpener.saveObj();
						this._nmOpener = undefined;
					}
					this._contentLoading();
				}, this));
			},

			// Resize the modal according to sizes.initW and sizes.initH
			// Will call size function
			// @param recalc boolean: Indicate if the size should be recalaculated (useful when content has changed)
			resize: function(recalc) {
				if (recalc) {
					this.elts.hidden.append(this.elts.cont.children().first().clone());
					this.sizes.initW = this.sizes.w = this.elts.hidden.width();
					this.sizes.initH = this.sizes.h = this.elts.hidden.height();
					this.elts.hidden.empty();
				} else {
					this.sizes.w = this.sizes.initW;
					this.sizes.h = this.sizes.initH;
				}
				this._unreposition();
				this.size();
				this._callAnim('resize', $.proxy(function() {
					this._reposition();
				}, this));
			},

			// Update sizes element to not go outsize the viewport.
			// Will call 'size' callback filter
			size: function() {
				var maxHeight = this.getInternal().fullSize.viewH - this.sizes.hMargin,
					maxWidth = this.getInternal().fullSize.viewW - this.sizes.wMargin;
				if (this.sizes.minW && this.sizes.minW > this.sizes.w)
					this.sizes.w = this.sizes.minW;
				if (this.sizes.minH && this.sizes.minH > this.sizes.h)
					this.sizes.h = this.sizes.minH;
				if (this.sizes.h > maxHeight || this.sizes.w > maxWidth) {
					// We're gonna resize the modal as it will goes outside the view port
					this.sizes.h = Math.min(this.sizes.h, maxHeight);
					this.sizes.w = Math.min(this.sizes.w, maxWidth);
				}
				this._callFilters('size');
			},

			// Get the nmObject for a new nyroModal
			getForNewLinks: function(elt) {
				var ret;
				if (this.stack && (!elt || this.isStackable(elt))) {
					ret = $.extend(true, {}, this);
					ret._nmOpener = undefined;
					ret.elts.all = undefined;
				} else {
					ret = $.extend({}, this);
					ret._nmOpener = this;
				}
				ret.filters = [];
				ret.opener = undefined;
				ret._open = false;
				return ret;
			},
			
			// Indicate if an element can be stackable or not, regarding the nonStackable setting
			isStackable: function(elt) {
				return !elt.is(this.nonStackable);
			},

			// key handle function.
			// Will call 'keyHandle' callback filter
			keyHandle: function(e) {
				this.keyEvent = e;
				this._callFilters('keyHandle');
				this.keyEvent = undefined;
				delete(this.keyEvent);
			},

			// Get the internal object
			getInternal: function() {
				return _internal;
			},

			// Internal function for closing a nyroModal
			// Will call 'close' callback filter
			_close: function() {
				this.getInternal()._removeStack(this.opener);
				this._opened = false;
				this._open = false;
				this._callFilters('close');
			},
			// Public function for closing a nyroModal
			close: function() {
				this._close();
				this._callFilters('beforeClose');
				var self = this;
				this._unreposition();
				self._callAnim('hideCont', function() {
					self._callAnim('hideLoad', function() {
						self._callAnim('hideBg', function() {
							self._callFilters('afterClose');
							self.elts.cont.remove();
							self.elts.hidden.remove();
							self.elts.load.remove();
							self.elts.bg.remove();
							self.elts.all.remove();
							self.elts.cont = self.elts.hidden = self.elts.load = self.elts.bg = self.elts.all = undefined;
						});
					});
				});
			},
			
			// Public function for destroying a nyroModal instance, only for non open modal
			destroy: function() {
				if (this._open)
					return false;
				this._callFilters('destroy');
				if (this.elts.all)
					this.elts.all.remove();
				return true;
			},

			// Init HTML elements
			_initElts: function() {
				if (!this.stack && this.getInternal().stack.length > 1)
					this.elts = this.getInternal().stack[this.getInternal().stack.length-2]['nmObj'].elts;
				if (!this.elts.all || this.elts.all.closest('body').length == 0)
					this.elts.all = this.elts.bg = this.elts.cont = this.elts.hidden = this.elts.load = undefined;
				if (!this.elts.all)
					this.elts.all = $('<div />').appendTo(this.getInternal()._container);
				if (!this.elts.bg)
					this.elts.bg = $('<div />').hide().appendTo(this.elts.all);
				if (!this.elts.cont)
					this.elts.cont = $('<div />').hide().appendTo(this.elts.all);
				if (!this.elts.hidden)
					this.elts.hidden = $('<div />').hide().appendTo(this.elts.all);
				this.elts.hidden.empty();
				if (!this.elts.load)
					this.elts.load = $('<div />').hide().appendTo(this.elts.all);
				this._callFilters('initElts');
			},

			// Trigger the error
			// Will call 'error' callback filter
			_error: function(jqXHR) {
				this._callFilters('error', jqXHR);
			},

			// Set the HTML content to show.
			// - html: HTML content
			// - selector: selector to filter the content
			// Will init the size and call the 'size' function.
			// Will call 'filledContent' callback filter
			_setCont: function(html, selector) {
				if (selector) {
					var tmp = [],
						i = 0;
					// Looking for script to store them
					html = html
						.replace(/\r\n/gi, 'nyroModalLN')
						.replace(/<script(.|\s)*?\/script>/gi, function(x) {
								tmp[i] = x;
								return '<pre class=nyroModalScript rel="'+(i++)+'"></pre>';
							});
					var cur = $('<div>'+html+'</div>').find(selector);
					if (cur.length) {
						html = cur.html()
							.replace(/<pre class="?nyroModalScript"? rel="?([0-9]*)"?><\/pre>/gi, function(x, y, z) { return tmp[y]; })
							.replace(/nyroModalLN/gi, "\r\n");
					} else {
						// selector not found
						this._error();
						return;
					}
				}
				this.elts.hidden
					.append(this._filterScripts(html))
					.prepend(this.header)
					.append(this.footer)
					.wrapInner($('<div />', {'class': 'nyroModal'+ucfirst(this.loadFilter)}));

				// Store the size of the element
				this.sizes.initW = this.sizes.w = this.elts.hidden.width();
				this.sizes.initH = this.sizes.h = this.elts.hidden.height();
				var outer = this.getInternal()._getOuter(this.elts.cont);
				this.sizes.hMargin = outer.h.total;
				this.sizes.wMargin = outer.w.total;

				this.size();

				this.loading = false;
				this._callFilters('filledContent');
				this._contentLoading();
			},

			// Filter an html content to remove the script[src] and store them appropriately if needed
			// - data: Data to filter
			_filterScripts: function(data) {
				if (typeof data != 'string')
					return data;

				this._scripts = [];
				this._scriptsShown = [];
				var start = 0,
					stStart = '<script',
					stEnd = '</script>',
					endLn = stEnd.length,
					pos,
					pos2,
					tmp;
				while ((pos = data.indexOf(stStart, start)) > -1) {
					pos2 = data.indexOf(stEnd)+endLn;
					tmp = $(data.substring(pos, pos2));
					if (!tmp.attr('src') || tmp.attr('rel') == 'forceLoad') {
						if (tmp.attr('rev') == 'shown')
							this._scriptsShown.push(tmp.get(0));
						else
							this._scripts.push(tmp.get(0));
					}
					data = data.substring(0, pos)+data.substr(pos2);
					start = pos;
				}
				return data;
			},

			// Check if the nmObject has a specific filter
			// - filter: Filter name
			_hasFilter: function(filter) {
				var ret = false;
				$.each(this.filters, function(i, f) {
					ret = ret || f == filter;
				});
				return ret;
			},

			// Remove a specific filter
			// - filter: Filter name
			_delFilter: function(filter) {
				this.filters = $.map(this.filters, function(v) {
					if (v != filter)
						return v;
				});
			},

			// Call a function against all active filters
			// - fct: Function name
			// - prm: Parameter to be used in callback
			// return an array of all return of callbacks; keys are filters name
			_callFilters: function(fct, prm) {
				this.getInternal()._debug(fct);
				var ret = [],
					self = this;
				$.each(this.filters, function(i, f) {
					ret[f] = self._callFilter(f, fct, prm);
				});
				if (this.callbacks[fct] && $.isFunction(this.callbacks[fct]))
					this.callbacks[fct](this, prm);
				return ret;
			},

			// Call a filter function for a specific filter
			// - f: Filter name
			// - fct: Function name
			// - prm: Parameter to be used in callback
			// return the return of the callback
			_callFilter: function(f, fct, prm) {
				if (_filters[f] && _filters[f][fct] && $.isFunction(_filters[f][fct]))
					return _filters[f][fct](this, prm);
				return undefined;
			},

			// Call animation callback.
			// Will also call beforeNNN and afterNNN filter callbacks
			// - fct: Animation function name
			// - clb: Callback once the animation is done
			_callAnim: function(fct, clb) {
				this.getInternal()._debug(fct);
				this._callFilters('before'+ucfirst(fct));
				if (!this._animated) {
					this._animated = true;
					if (!$.isFunction(clb)) clb = $.noop;
					if (this.anims[fct] && $.isFunction(this.anims[fct])) {
						curFct = this.anims[fct];
					} else {
						var set = this.anim[fct] || this.anim.def || 'basic';
						if (!_animations[set] || !_animations[set][fct] || !$.isFunction(_animations[set][fct]))
							set = 'basic';
						curFct = _animations[set][fct];
					}
					curFct(this, $.proxy(function() {
							this._animated = false;
							this._callFilters('after'+ucfirst(fct));
							clb();
						}, this));
				}
			},

			// Load the content
			// Will call the 'load' function of the filter specified in the loadFilter parameter
			_load: function() {
				this.getInternal()._debug('_load');
				if (!this.loading && this.loadFilter) {
					this.loading = true;
					this._callFilter(this.loadFilter, 'load');
				}
			},

			// Show the content or the loading according to the current state of the modal
			_contentLoading: function() {
				if (!this._animated && this._bgReady) {
					if (!this._transition && this.elts.cont.html().length > 0)
						this._transition = true;
					this._nbContentLoading++;
					if (!this.loading) {
						if (!this._opened) {
							this._opened = true;
							if (this._transition) {
								var fct = $.proxy(function() {
									this._writeContent();
									this._callFilters('beforeShowCont');
									this._callAnim('hideTrans', $.proxy(function() {
										this._transition = false;
										this._callFilters('afterShowCont');
										this.elts.cont.append(this._scriptsShown);
										this._reposition();
										this.elts.cont.scrollTop(0);
									}, this));
								}, this);
								if (this._nbContentLoading == 1) {
									this._unreposition();
									this._callAnim('showTrans', fct);
								} else {
									fct();
								}
							} else {
								this._callAnim('hideLoad', $.proxy(function() {
									this._writeContent();
									this._callAnim('showCont', $.proxy(function() {
										this.elts.cont.append(this._scriptsShown);
										this._reposition();
										this.elts.cont.scrollTop(0);
									}, this));
								}, this));
							}
						}
					} else if (this._nbContentLoading == 1) {
						var outer = this.getInternal()._getOuter(this.elts.load);
						this.elts.load
							.css({
								position: 'fixed',
								top: (this.getInternal().fullSize.viewH - this.elts.load.height() - outer.h.margin)/2,
								left: (this.getInternal().fullSize.viewW - this.elts.load.width() - outer.w.margin)/2
							});
						if (this._transition) {
							this._unreposition();
							this._callAnim('showTrans', $.proxy(function() {
								this._contentLoading();
							}, this));
						} else {
							this._callAnim('showLoad', $.proxy(function() {
								this._contentLoading();
							}, this));
						}
					}
				}
			},

			// Write the content in the modal.
			// Content comes from the hidden div, scripts and eventually close button.
			_writeContent: function() {
				this.elts.cont
					.empty()
					.append(this.elts.hidden.contents())
					.append(this._scripts)
					.append(this.showCloseButton ? this.closeButton : '')
					.css({
						position: 'fixed',
						width: this.sizes.w,
						height: this.sizes.h,
						top: (this.getInternal().fullSize.viewH - this.sizes.h - this.sizes.hMargin)/2,
						left: (this.getInternal().fullSize.viewW - this.sizes.w - this.sizes.wMargin)/2
					});
			},

			// Reposition elements with a class nmReposition
			_reposition: function() {
				var elts = this.elts.cont.find('.nmReposition');
				if (elts.length) {
					var space = this.getInternal()._getSpaceReposition();
					elts.each(function() {
						var me = $(this),
							offset = me.offset();
						me.css({
							position: 'fixed',
							top: offset.top - space.top,
							left: offset.left - space.left
						});
					});
					this.elts.cont.after(elts);
				}
				this.elts.cont.css('overflow', 'auto');
				this._callFilters('afterReposition');
			},

			// Unreposition elements with a class nmReposition
			// Exaclty the reverse of the _reposition function
			_unreposition: function() {
				this.elts.cont.css('overflow', '');
				var elts = this.elts.all.find('.nmReposition');
				if (elts.length)
					this.elts.cont.append(elts.removeAttr('style'));
				this._callFilters('afterUnreposition');
			}
		},
		_internal = {
			firstInit: true,
			debug: false,
			stack: [],
			fullSize: {
				w: 0,
				h: 0,
				wW: 0,
				wH: 0,
				viewW: 0,
				viewH: 0
			},
			nyroModal: function(opts, fullObj) {
				if (_internal.firstInit) {
					_internal._container = $('<div />').appendTo($b);
					$w.smartresize($.proxy(_internal._resize, _internal));
					$d.on('keydown.nyroModal', $.proxy(_internal._keyHandler, _internal));
					_internal._calculateFullSize();
					_internal.firstInit = false;
				}
				return this.nmInit(opts, fullObj).each(function() {
					_internal._init($(this).data('nmObj'));
				});
			},
			nmInit: function(opts, fullObj) {
				return this.each(function() {
					var me = $(this);
					if (fullObj)
						me.data('nmObj', $.extend(true, {opener: me}, opts));
					else
						me.data('nmObj',
							me.data('nmObj')
								? $.extend(true, me.data('nmObj'), opts)
								: $.extend(true, {opener: me}, _nmObj, opts));
				});
			},
			nmDestroy: function() {
				return this.each(function() {
					var me = $(this);
					if (me.data('nmObj')) {
						if (me.data('nmObj').destroy())
							me.removeData('nmObj');
					}
				});
			},
			nmCall: function() {
				return this.trigger('nyroModal');
			},

			nmManual: function(url, opts) {
				$('<a />', {href: url}).nyroModal(opts).trigger('nyroModal');
			},
			nmData: function(data, opts) {
				this.nmManual('#', $.extend({data: data}, opts));
			},
			nmObj: function(opts) {
				$.extend(true, _nmObj, opts);
			},
			nmInternal: function(opts) {
				$.extend(true, _internal, opts);
			},
			nmAnims: function(opts) {
				$.extend(true, _animations, opts);
			},
			nmFilters: function(opts) {
				$.extend(true, _filters, opts);
			},
			nmTop: function() {
				if (_internal.stack.length)
					return _internal.stack[_internal.stack.length-1]['nmObj'];
				return undefined;
			},

			_debug: function(msg) {
				if (this.debug && window.console && window.console.log)
					window.console.log(msg);
			},

			_container: undefined,

			_init: function(nm) {
				nm.filters = [];
				$.each(_filters, function(f, obj) {
					if (obj.is && $.isFunction(obj.is) && obj.is(nm)) {
						nm.filters.push(f);
					}
				});
				nm._callFilters('initFilters');
				nm._callFilters('init');
				nm.opener
					.off('nyroModal.nyroModal nmClose.nyroModal nmResize.nyroModal')
					.on({
						'nyroModal.nyroModal': 	function() { nm.open(); return false;},
						'nmClose.nyroModal': 	function() { nm.close(); return false;},
						'nmResize.nyroModal': 	function() { nm.resize(); return false;}
					});
			},

			_scrollWidth: (function() {
				var scrollbarWidth;
				if ($.browser.msie) {
					var $textarea1 = $('<textarea cols="10" rows="2"></textarea>')
							.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo($b),
						$textarea2 = $('<textarea cols="10" rows="2" style="overflow: hidden;"></textarea>')
							.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo($b);
					scrollbarWidth = $textarea1.width() - $textarea2.width();
					$textarea1.add($textarea2).remove();
				} else {
					var $div = $('<div />')
						.css({ width: 100, height: 100, overflow: 'auto', position: 'absolute', top: -1000, left: -1000 })
						.prependTo($b).append('<div />').find('div')
							.css({ width: '100%', height: 200 });
					scrollbarWidth = 100 - $div.width();
					$div.parent().remove();
				}
				return scrollbarWidth;
			})(),

			_selNyroModal: function(obj) {
				return $(obj).data('nmObj') ? true : false;
			},

			_selNyroModalOpen: function(obj) {
				var me = $(obj);
				return me.data('nmObj') ? me.data('nmObj')._open : false;
			},

			_keyHandler: function(e) {
				var nmTop = $.nmTop();
				if (nmTop && nmTop.useKeyHandler) {
					return nmTop.keyHandle(e);
				}
			},
			_pushStack: function(obj) {
				this.stack = $.map(this.stack, function(elA) {
					if (elA['nmOpener'] != obj.get(0))
						return elA;
				});
				this.stack.push({
					nmOpener: obj.get(0),
					nmObj: $(obj).data('nmObj')
				});
			},
			_removeStack: function(obj) {
				this.stack = $.map(this.stack, function(elA) {
					if (elA['nmOpener'] != obj.get(0))
						return elA;
				});
			},
			_resize: function() {
				var opens = $(':nmOpen').each(function() {
					$(this).data('nmObj')._unreposition();
				});
				this._calculateFullSize();
				opens.trigger('nmResize');
			},
			_calculateFullSize: function() {
				this.fullSize = {
					w: $d.width(),
					h: $d.height(),
					wW: $w.width(),
					wH: $w.height()
				};
				this.fullSize.viewW = Math.min(this.fullSize.w, this.fullSize.wW);
				this.fullSize.viewH = Math.min(this.fullSize.h, this.fullSize.wH);
			},
			_getCurCSS: function(elm, name) {
				var ret = parseInt($.css(elm, name, true));
				return isNaN(ret) ? 0 : ret;
			},
			_getOuter: function(elm) {
				elm = elm.get(0);
				var ret = {
					h: {
						margin: this._getCurCSS(elm, 'marginTop') + this._getCurCSS(elm, 'marginBottom'),
						border: this._getCurCSS(elm, 'borderTopWidth') + this._getCurCSS(elm, 'borderBottomWidth'),
						padding: this._getCurCSS(elm, 'paddingTop') + this._getCurCSS(elm, 'paddingBottom')
					},
					w: {
						margin: this._getCurCSS(elm, 'marginLeft') + this._getCurCSS(elm, 'marginRight'),
						border: this._getCurCSS(elm, 'borderLeftWidth') + this._getCurCSS(elm, 'borderRightWidth'),
						padding: this._getCurCSS(elm, 'paddingLeft') + this._getCurCSS(elm, 'paddingRight')
					}
				};

				ret.h.outer = ret.h.margin + ret.h.border;
				ret.w.outer = ret.w.margin + ret.w.border;

				ret.h.inner = ret.h.padding + ret.h.border;
				ret.w.inner = ret.w.padding + ret.w.border;

				ret.h.total = ret.h.outer + ret.h.padding;
				ret.w.total = ret.w.outer + ret.w.padding;

				return ret;
			},
			_getSpaceReposition: function() {
				var	outer = this._getOuter($b),
					ie7 = $.browser.msie && $.browser.version < 8 && !(screen.height <= $w.height()+23);
				return {
					top: $w.scrollTop() - (!ie7 ? outer.h.border / 2 : 0),
					left: $w.scrollLeft() - (!ie7 ? outer.w.border / 2 : 0)
				};
			},

			_getHash: function(url) {
				if (typeof url == 'string') {
					var hashPos = url.indexOf('#');
					if (hashPos > -1)
						return url.substring(hashPos);
				}
				return '';
			},
			_extractUrl: function(url) {
				var ret = {
					url: undefined,
					sel: undefined
				};

				if (url) {
					var hash = this._getHash(url),
						hashLoc = this._getHash(window.location.href),
						curLoc = window.location.href.substring(0, window.location.href.length - hashLoc.length),
						req = url.substring(0, url.length - hash.length);
					ret.sel = hash;
					if (req != curLoc && req != baseHref)
						ret.url = req;
				}
				return ret;
			}
		},
		_animations = {
			basic: {
				showBg: function(nm, clb) {
					nm.elts.bg.css({opacity: 0.7}).show();
					clb();
				},
				hideBg: function(nm, clb) {
					nm.elts.bg.hide();
					clb();
				},
				showLoad: function(nm, clb) {
					nm.elts.load.show();
					clb();
				},
				hideLoad: function(nm, clb) {
					nm.elts.load.hide();
					clb();
				},
				showCont: function(nm, clb) {
					nm.elts.cont.show();
					clb();
				},
				hideCont: function(nm, clb) {
					nm.elts.cont.hide();
					clb();
				},
				showTrans: function(nm, clb) {
					nm.elts.cont.hide();
					nm.elts.load.show();
					clb();
				},
				hideTrans: function(nm, clb) {
					nm.elts.cont.show();
					nm.elts.load.hide();
					clb();
				},
				resize: function(nm, clb) {
					nm.elts.cont.css({
						width: nm.sizes.w,
						height: nm.sizes.h,
						top: (nm.getInternal().fullSize.viewH - nm.sizes.h - nm.sizes.hMargin)/2,
						left: (nm.getInternal().fullSize.viewW - nm.sizes.w - nm.sizes.wMargin)/2
					});
					clb();
				}
			}
		},
		_filters = {
			basic: {
				is: function(nm) {
					return true;
				},
				init: function(nm) {
					if (nm.opener.attr('rev') == 'modal')
						nm.modal = true;
					if (nm.modal)
						nm.closeOnEscape = nm.closeOnClick = nm.showCloseButton = false;
					if (nm.closeOnEscape)
						nm.useKeyHandler = true;
				},
				initElts: function(nm) {
					nm.elts.bg.addClass('nyroModalBg');
					if (nm.closeOnClick)
						nm.elts.bg.off('click.nyroModal').on('click.nyroModal', function(e) {
							e.preventDefault();
							nm.close();
						});
					nm.elts.cont.addClass('nyroModalCont');
					nm.elts.hidden.addClass('nyroModalCont nyroModalHidden');
					nm.elts.load.addClass('nyroModalCont nyroModalLoad');
				},
				error: function(nm) {
					nm.elts.hidden.addClass('nyroModalError');
					nm.elts.cont.addClass('nyroModalError');
					nm._setCont(nm.errorMsg);
				},
				beforeShowCont: function(nm) {
					nm.elts.cont
						.find('.nyroModal').each(function() {
							var cur = $(this);
							cur.nyroModal(nm.getForNewLinks(cur), true);
						}).end()
						.find('.nyroModalClose').on('click.nyroModal', function(e) {
							e.preventDefault();
							nm.close();
						});
				},
				keyHandle: function(nm) {
					// used for escape key
					if (nm.keyEvent.keyCode == 27 && nm.closeOnEscape) {
						nm.keyEvent.preventDefault();
						nm.close();
					}
				}
			},

			custom: {
				is: function(nm) {
					return true;
				}
			}
		};

	// Add jQuery call fucntions
	$.fn.extend({
		nm: _internal.nyroModal,
		nyroModal: _internal.nyroModal,
		nmInit: _internal.nmInit,
		nmDestroy: _internal.nmDestroy,
		nmCall: _internal.nmCall
	});

	// Add global jQuery functions
	$.extend({
		nmManual: _internal.nmManual,
		nmData: _internal.nmData,
		nmObj: _internal.nmObj,
		nmInternal: _internal.nmInternal,
		nmAnims: _internal.nmAnims,
		nmFilters: _internal.nmFilters,
		nmTop: _internal.nmTop
	});

	// Add jQuery selectors
	$.expr[':'].nyroModal = $.expr[':'].nm = _internal._selNyroModal;
	$.expr[':'].nmOpen = _internal._selNyroModalOpen;
});

// Smartresize plugin
(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  };
	// smartresize
	jQuery.fn[sr] = function(fn){  return fn ? this.on('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
// ucFirst
function ucfirst(str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: ucfirst('kevin van zonneveld');
    // *     returns 1: 'Kevin van zonneveld'
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}
/*
 * nyroModal v2.0.0
 * 
 * Fade animations
 * 
 * Depends:
 * 
 */
jQuery(function($, undefined) {
	$.nmAnims({
		fade: {
			showBg: function(nm, clb) {
				nm.elts.bg.fadeTo(250, 0.7, clb);
			},
			hideBg: function(nm, clb) {
				nm.elts.bg.fadeOut(clb);
			},
			showLoad: function(nm, clb) {
				nm.elts.load.fadeIn(clb);
			},
			hideLoad: function(nm, clb) {
				nm.elts.load.fadeOut(clb);
			},
			showCont: function(nm, clb) {
				nm.elts.cont.fadeIn(clb);
			},
			hideCont: function(nm, clb) {
				nm.elts.cont.css('overflow', 'hidden').fadeOut(clb);
			},
			showTrans: function(nm, clb) {
				nm.elts.load
					.css({
						position: nm.elts.cont.css('position'),
						top: nm.elts.cont.css('top'),
						left: nm.elts.cont.css('left'),
						width: nm.elts.cont.css('width'),
						height: nm.elts.cont.css('height'),
						marginTop: nm.elts.cont.css('marginTop'),
						marginLeft: nm.elts.cont.css('marginLeft')
					})
					.fadeIn(function() {
						nm.elts.cont.hide();
						clb();
					});
			},
			hideTrans: function(nm, clb) {
				nm.elts.cont.css('visibility', 'hidden').show();
				nm.elts.load
					.css('position', nm.elts.cont.css('position'))
					.animate({
						top: nm.elts.cont.css('top'),
						left: nm.elts.cont.css('left'),
						width: nm.elts.cont.css('width'),
						height: nm.elts.cont.css('height'),
						marginTop: nm.elts.cont.css('marginTop'),
						marginLeft: nm.elts.cont.css('marginLeft')
					}, function() {
						nm.elts.cont.css('visibility', '');
						nm.elts.load.fadeOut(clb);
					});
			},
			resize: function(nm, clb) {
				nm.elts.cont.animate({
					width: nm.sizes.w,
					height: nm.sizes.h,
					top: (nm.getInternal().fullSize.viewH - nm.sizes.h - nm.sizes.hMargin)/2,
					left: (nm.getInternal().fullSize.viewW - nm.sizes.w - nm.sizes.wMargin)/2
				}, clb);
			}
		}
	});
	// Define fade aniamtions as default
	$.nmObj({anim: {def: 'fade'}});
});
/*
 * nyroModal v2.0.0
 * 
 * Title filter
 * 
 * Depends:
 * 
 * Before:
 */
jQuery(function($, undefined) {
	$.nmFilters({
		title: {
			is: function(nm) {
				return nm.opener.is('[title]');
			},
			beforeShowCont: function(nm) {
				var offset = nm.elts.cont.offset();
				nm.store.title = $('<h1 />', {
					text: nm.opener.attr('title')
				}).addClass('nyroModalTitle nmReposition');
				nm.elts.cont.prepend(nm.store.title);
			},
			close: function(nm) {
				if (nm.store.title) {
					nm.store.title.remove();
					nm.store.title = undefined;
					delete(nm.store.title);
				}
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Gallery filter
 * 
 * Depends:
 * - filters.title
 * 
 * Before: filters.title
 */
jQuery(function($, undefined) {
	$.nmFilters({
		gallery: {
			is: function(nm) {
				var ret = nm.opener.is('[rel]:not([rel=external], [rel=nofollow])');
				if (ret) {
					var rel = nm.opener.attr('rel'),
						indexSpace = rel.indexOf(' '),
						gal = indexSpace > 0 ? rel.substr(0, indexSpace) : rel,
						links = $('[href][rel="'+gal+'"], [href][rel^="'+gal+' "]');
					if (links.length < 2)
						ret = false;
					if (ret && nm.galleryCounts && !nm._hasFilter('title'))
						nm.filters.push('title');
				}
				return ret;
			},
			init: function(nm) {
				nm.useKeyHandler = true;
			},
			keyHandle: function(nm) {
				// used for arrows key
				if (!nm._animated && nm._opened) {
					if (nm.keyEvent.keyCode == 39 || nm.keyEvent.keyCode == 40) {
						nm.keyEvent.preventDefault();
						nm._callFilters('galleryNext');
					} else if (nm.keyEvent.keyCode == 37 || nm.keyEvent.keyCode == 38) {
						nm.keyEvent.preventDefault();
						nm._callFilters('galleryPrev');
					}
				}
			},
			initElts: function(nm) {
				var rel = nm.opener.attr('rel'),
					indexSpace = rel.indexOf(' ');
				nm.store.gallery = indexSpace > 0 ? rel.substr(0, indexSpace) : rel;
				nm.store.galleryLinks = $('[href][rel="'+nm.store.gallery+'"], [href][rel^="'+nm.store.gallery+' "]');
				nm.store.galleryIndex = nm.store.galleryLinks.index(nm.opener);
			},
			beforeShowCont: function(nm) {
				if (nm.galleryCounts && nm.store.title && nm.store.galleryLinks && nm.store.galleryLinks.length > 1) {
					var curTitle = nm.store.title.html();
					nm.store.title.html((curTitle.length ? curTitle+' - ' : '')+(nm.store.galleryIndex+1)+'/'+nm.store.galleryLinks.length);
				}
			},
			filledContent: function(nm) {
				var link = this._getGalleryLink(nm, -1),
					append = nm.elts.hidden.find(' > div');
				if (link) {
					$('<a />', {
							text: 'previous',
							href: '#'
						})
						.addClass('nyroModalPrev')
						.on('click', function(e) {
							e.preventDefault();
							nm._callFilters('galleryPrev');
						})
						.appendTo(append);
				}
				link = this._getGalleryLink(nm, 1);
				if (link) {
					$('<a />', {
							text: 'next',
							href: '#'
						})
						.addClass('nyroModalNext')
						.on('click', function(e) {
							e.preventDefault();
							nm._callFilters('galleryNext');
						})
						.appendTo(append);
				}
			},
			close: function(nm) {
				nm.store.gallery = undefined;
				nm.store.galleryLinks = undefined;
				nm.store.galleryIndex = undefined;
				delete(nm.store.gallery);
				delete(nm.store.galleryLinks);
				delete(nm.store.galleryIndex);
				if (nm.elts.cont)
					nm.elts.cont.find('.nyroModalNext, .nyroModalPrev').remove();
			},
			galleryNext: function(nm) {
				this._getGalleryLink(nm, 1).nyroModal(nm.getForNewLinks(), true).click();
			},
			galleryPrev: function(nm) {
				this._getGalleryLink(nm, -1).nyroModal(nm.getForNewLinks(), true).click();
			},
			_getGalleryLink: function(nm, dir) {
				if (nm.store.gallery) {
					if (!nm.ltr)
						dir *= -1;
					var index = nm.store.galleryIndex + dir;
					if (nm.store.galleryLinks && index >= 0 && index < nm.store.galleryLinks.length)
						return nm.store.galleryLinks.eq(index);
					else if (nm.galleryLoop && nm.store.galleryLinks)
						return nm.store.galleryLinks.eq(index<0 ? nm.store.galleryLinks.length-1 : 0);
				}
				return undefined;
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Link filter
 * 
 * Depends:
 * 
 * Before: filters.gallery
 */
jQuery(function($, undefined) {
	$.nmFilters({
		link: {
			is: function(nm) {
				var ret = nm.opener.is('[href]');
				if (ret)
					nm.store.link = nm.getInternal()._extractUrl(nm.opener.attr('href'));
				return ret;
			},
			init: function(nm) {
				nm.loadFilter = 'link';
				nm.opener.off('click.nyroModal').on('click.nyroModal', function(e) {
					e.preventDefault();
					nm.opener.trigger('nyroModal');
				});
			},
			load: function(nm) {
				$.ajax($.extend(true, {}, nm.ajax || {}, {
					url: nm.store.link.url,
					data: nm.store.link.sel ? [{name: nm.selIndicator, value: nm.store.link.sel.substring(1)}] : undefined,
					success: function(data) {
						nm._setCont(data, nm.store.link.sel);
					},
					error: function(jqXHR) {
						nm._error(jqXHR);
					}
				}));
			},
			destroy: function(nm) {
				nm.opener.off('click.nyroModal');
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Dom filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.link
 */
jQuery(function($, undefined) {
	$.nmFilters({
		dom: {
			is: function(nm) {
				return nm._hasFilter('link') && !nm.store.link.url && nm.store.link.sel;
			},
			init: function(nm) {
				nm.loadFilter = 'dom';
			},
			load: function(nm) {
				nm.store.domEl = $(nm.store.link.sel);
				if (nm.store.domEl.length)
					nm._setCont(nm.domCopy ? nm.store.domEl.html() : nm.store.domEl.contents());
				else
					nm._error();
			},
			close: function(nm) {
				if (!nm.domCopy && nm.store.domEl && nm.elts.cont)
					nm.store.domEl.append(nm.elts.cont.find('.nyroModalDom').contents());
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Data filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.dom
 */
jQuery(function($, undefined) {
	$.nmFilters({
		data: {
			is: function(nm) {
				var ret = nm.data ? true : false;
				if (ret)
					nm._delFilter('dom');
				return ret;
			},
			init: function(nm) {
				nm.loadFilter = 'data';
			},
			load: function(nm) {
				nm._setCont(nm.data);
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Image filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.data
 */
jQuery(function($, undefined) {
	$.nmFilters({
		image: {
			is: function(nm) {
				return (new RegExp(nm.imageRegex, 'i')).test(nm.opener.attr('href'));
			},
			init: function(nm) {
				nm.loadFilter = 'image';
			},
			load: function(nm) {
				var url = nm.opener.attr('href');
				$('<img />')
					.load(function() {
						nm.elts.cont.addClass('nyroModalImg');
						nm.elts.hidden.addClass('nyroModalImg');
						nm._setCont(this);
					}).error(function() {
						nm._error();
					})
					.attr('src', url);
			},
			size: function(nm) {
				if (nm.sizes.w != nm.sizes.initW || nm.sizes.h != nm.sizes.initH) {
					var ratio = Math.min(nm.sizes.w/nm.sizes.initW, nm.sizes.h/nm.sizes.initH);
					nm.sizes.w = nm.sizes.initW * ratio;
					nm.sizes.h = nm.sizes.initH * ratio;
				}
				var img = nm.loading ? nm.elts.hidden.find('img') : nm.elts.cont.find('img');
				img.attr({
					width: nm.sizes.w,
					height: nm.sizes.h
				});
			},
			close: function(nm) {
				if (nm.elts.cont) {
					nm.elts.cont.removeClass('nyroModalImg');
					nm.elts.hidden.removeClass('nyroModalImg');
				}
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * SWF filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.image
 */
jQuery(function($, undefined) {
	$.nmFilters({
		swf: {
			idCounter: 1,
			is: function(nm) {
				return nm._hasFilter('link') && nm.opener.is('[href$=".swf"]');
			},
			init: function(nm) {
				nm.loadFilter = 'swf';
			},
			load: function(nm) {
				if (!nm.swfObjectId)
					nm.swfObjectId = 'nyroModalSwf-'+(this.idCounter++);
				var url = nm.store.link.url,
					cont = '<div><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="'+nm.swfObjectId+'" width="'+nm.sizes.w+'" height="'+nm.sizes.h+'"><param name="movie" value="'+url+'"></param>',
					tmp = '';
				$.each(nm.swf, function(name, val) {
					cont+= '<param name="'+name+'" value="'+val+'"></param>';
					tmp+= ' '+name+'="'+val+'"';
				});
				cont+= '<embed src="'+url+'" type="application/x-shockwave-flash" width="'+nm.sizes.w+'" height="'+nm.sizes.h+'"'+tmp+'></embed></object></div>';
				nm._setCont(cont);
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Iframe filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.formFile
 */
jQuery(function($, undefined) {
	$.nmFilters({
		iframe: {
			is: function(nm) {
				var	target = nm.opener.attr('target') || '',
					rel = nm.opener.attr('rel') || '',
					opener = nm.opener.get(0);
				return !nm._hasFilter('image') && (target.toLowerCase() == '_blank'
					|| rel.toLowerCase().indexOf('external') > -1
					|| (opener.hostname && opener.hostname.replace(/:\d*$/,'') != window.location.hostname.replace(/:\d*$/,'')));
			},
			init: function(nm) {
				nm.loadFilter = 'iframe';
			},
			load: function(nm) {
				nm.store.iframe = $('<iframe />')
					.attr({
						src: 'javascript:\'\';',
						id: 'nyromodal-iframe-'+(new Date().getTime()),
						frameborder: '0'
					});
				nm._setCont(nm.store.iframe);
			},
			afterShowCont: function(nm) {
				nm.store.iframe.attr('src', nm.opener.attr('href'));
			},
			close: function(nm) {
				if (nm.store.iframe) {
					nm.store.iframe.remove();
					nm.store.iframe = undefined;
					delete(nm.store.iframe);
				}
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Iframe form filter
 * 
 * Depends:
 * - filters.iframe
 * 
 * Before: filters.iframe
 */
jQuery(function($, undefined) {
	$.nmFilters({
		iframeForm: {
			is: function(nm) {
				var ret = nm._hasFilter('iframe') && nm.opener.is('form');
				if (ret) {
					nm._delFilter('iframe');
					nm._delFilter('form');
				}
				return ret;
			},
			init: function(nm) {
				nm.loadFilter = 'iframeForm';
				nm.store.iframeFormLoading = false;
				nm.store.iframeFormOrgTarget = nm.opener.attr('target');
				nm.opener.off('submit.nyroModal').on('submit.nyroModal', function(e) {
					if (!nm.store.iframeFormIframe) {
						e.preventDefault();
						nm.opener.trigger('nyroModal');
					} else {
						nm.store.iframeFormLoading = true;
					}
				});
			},
			load: function(nm) {
				nm.store.iframeFormIframe = $('<iframe />')
					.attr({
						name: 'nyroModalIframeForm',
						src: 'javascript:\'\';',
						id: 'nyromodal-iframe-'+(new Date().getTime()),
						frameborder: '0'
					});
				nm._setCont(nm.store.iframeFormIframe);
			},
			afterShowCont: function(nm) {
				nm.opener
					.attr('target', 'nyroModalIframeForm')
					.submit();
			},
			close: function(nm) {
				nm.store.iframeFormOrgTarget ? nm.opener.attr('target', nm.store.iframeFormOrgTarget) : nm.opener.removeAttr('target');
				delete(nm.store.formFileLoading);
				delete(nm.store.iframeFormOrgTarget);
				if (nm.store.iframeFormIframe) {
					nm.store.iframeFormIframe.remove();
					nm.store.iframeFormIframe = undefined;
					delete(nm.store.iframeFormIframe);
				}
			},
			destroy: function(nm) {
				nm.opener.off('submit.nyroModal')
			}
		}
	});
});
/*
 * nyroModal v2.0.0
 * 
 * Embedly filter
 * 
 * Depends:
 * - filters.link
 * 
 * Before: filters.iframeForm
 */
jQuery(function($, undefined) {
	$.nmObj({
		embedlyUrl: 'http://api.embed.ly/1/oembed',
		embedly: {
			key: undefined,
			wmode: 'transparent',
			allowscripts: true,
			format: 'json'
			
			/*
			maxwidth: 400,
			maxheight: 400,
			width: 400,
			nostyle: false,
			autoplay: false,
			videosrc: false,
			words: 50,
			chars: 100
			*/
		}
	});
	var cache = [];
	$.nmFilters({
		embedly: {
			is: function(nm) {
				if (nm._hasFilter('link') && nm._hasFilter('iframe') && nm.opener.attr('href') && nm.embedly.key) {
					if (cache[nm.opener.attr('href')]) {
						nm.store.embedly = cache[nm.opener.attr('href')];
						nm._delFilter('iframe');
						return true;
					}
					nm.store.embedly = false;
					var data = nm.embedly;
					data.url = nm.opener.attr('href');
					$.ajax({
						url: nm.embedlyUrl,
						dataType: 'jsonp',
						data: data,
						success: function(data) {
							if (data.type != 'error' && data.html) {
								nm.store.embedly = data;
								cache[nm.opener.attr('href')] = data;
								nm._delFilter('iframe');
								nm.filters.push('embedly');
								nm._callFilters('initFilters');
								nm._callFilters('init');
							}
						}
					});
				}
				return false;
			},
			init: function(nm) {
				nm.loadFilter = 'embedly';
			},
			load: function(nm) {
				if (nm.store.embedly.type == 'photo') {
					nm.filters.push('image');
					$('<img />')
						.load(function() {
							nm.elts.cont.addClass('nyroModalImg');
							nm.elts.hidden.addClass('nyroModalImg');
							nm._setCont(this);
						}).error(function() {
							nm._error();
						})
						.attr('src', nm.store.embedly.url);
				} else {
					nm._setCont('<div>'+nm.store.embedly.html+'</div>');
				}
			},
			size: function(nm) {
				if (nm.store.embedly.width && !nm.sizes.height) {
					nm.sizes.w = nm.store.embedly.width;
					nm.sizes.h = nm.store.embedly.height;
				}
			}
		}
	});
});

/*
 * hScroll - Plugin for Jquery that controls horizontal scrolling
 *
 * Copyright (c) 2010 Heerko van der Kooij (swummoq.net)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 */

;
(function($, window, document, undefined) {
	var pluginName = 'browseList',
		defaults = {
			'page_size': 3,
			'element_selector': 'li',
			'loop': true,
			'onInitDone': null,
			'beforeElementDisplay': null,
			'onDisplayDone': null
		};

	function Plugin(element, opts) {
		this.element = $(element);
		this.options = $.extend({}, defaults, opts);
		this.page = 0;
		this.pages = 0;
		this.els = [];
		this._defaults = defaults;
		this._name = pluginName;
		this.init();
	}

	Plugin.prototype.init = function() {
		this.els = this.element.find(this.options.element_selector);
		this.pages = Math.ceil(this.els.length / this.options.page_size);
		this.displayCurrentPage();
		if ( 'function' == typeof this.options.onInitDone ){
			this.options.onInitDone.call( this, this, this.element, this.els );
		}
	}

	Plugin.prototype.displayCurrentPage = function() {
		this.els.hide();
		var start = this.page * this.options.page_size,
			end = Math.min(start + this.options.page_size, this.els.length);
		for (i = start; i < end; i++) {
			var el = $(this.els[i]);
			if ( ! el.data( pluginName + '_inited' ) ){
				el.data( pluginName + '_inited', true );
				if ( 'function' == typeof this.options.beforeElementDisplay ){
					this.options.beforeElementDisplay.call( this, el );
				}
			}
			el.fadeIn();
		}
		if ( 'function' == typeof this.options.onDisplayDone ){
			this.options.onDisplayDone.call( this, this, this.element, this.els );
		}
	}

	Plugin.prototype.next = function() {
		if (this.page >= this.pages - 1) {
			if (this.options.loop) {
				this.page = 0;
			} else {
				return -1;
			}
		} else this.page++;
		this.displayCurrentPage();
		return ( this.page < this.pages - 1 || this.options.loop );
	}

	Plugin.prototype.prev = function() {
		if (this.page <= 0) {
			if (this.options.loop) {
				this.page = this.pages - 1;
			} else {
				return false;
			}
		} else this.page--;
		this.displayCurrentPage();
		return ( this.page > 0 || this.options.loop );
	}

	Plugin.prototype.jumptoItem = function( showItem ) {
		var newpage = Math.floor( ( showItem - 1 ) / this.options.page_size );
		if ( newpage != this.page ){
			this.page = newpage;
			this.displayCurrentPage();
		}
	}

	Plugin.prototype.getState = function() {
		return state = {
			totalPages: this.pages,
			currentPage: this.page,
			hasNext: ( this.page < this.pages - 1 || this.options.loop ),
			hasPrev: ( this.page > 0 || this.options.loop ),
			loop: this.loop
		}
	}

	$.fn[pluginName] = function(args) {
		return this.each(function() {
			var inst, options = args;
			if (typeof args === 'object' || undefined === args) {
				if (!$.data(this, 'plugin_' + pluginName)) {
					inst = new Plugin(this, options);
					$.data(this, 'plugin_' + pluginName, inst);
				}
			} else if (typeof args === 'string' && Plugin.prototype[args]) {
				if (!$.data(this, 'plugin_' + pluginName)) {
					$.error('Initialize the plugin before calling "' + args + '"');
					return;
				}
				inst = $.data(this, 'plugin_' + pluginName);
				return inst[args].apply(inst, Array.prototype.slice.call(arguments, 1));
			} else {
				$.error('Method ' + args + ' does not exist on jQuery.' + pluginName);
			}
		});
	};

}(jQuery, window, document));
/*!
 * jQuery Cookie Plugin v1.3
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function ($, document, undefined) {

	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	var config = $.cookie = function (key, value, options) {

		// write
		if (value !== undefined) {
			options = $.extend({}, config.defaults, options);

			if (value === null) {
				options.expires = -1;
			}

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = config.json ? JSON.stringify(value) : String(value);

			return (document.cookie = [
				encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// read
		var decode = config.raw ? raw : decoded;
		var cookies = document.cookie.split('; ');
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			if (decode(parts.shift()) === key) {
				var cookie = decode(parts.join('='));
				return config.json ? JSON.parse(cookie) : cookie;
			}
		}

		return null;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) !== null) {
			$.cookie(key, null, options);
			return true;
		}
		return false;
	};

})(jQuery, document);

// usage: log('inside coolFunc',this,arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function() {
	window.log.history = window.log.history || []; // store logs to an array for reference
	window.log.history.push(arguments);
	if (this.console) {
		window.console.log(Array.prototype.slice.call(arguments));
	}
};

/*jshint multistr: true */

/**
 * AppMain entrypoint.
 * Handles registering and initializing the modules.
 * Handles event pub/sub system for the modules.
 **/
var AppMain = (function($) {
	'use strict';

	var initialized = false,
		initializers = [],
		my = {
			$: $,
			settings: {
				templates:{
					nav_list: '<ul class="%s">\
			<li class="prev"><span>prev</span></li>\
			<li class="next"><span>next</span></li>\
		</ul>'
				}
			},
			modules: {

			}
		},
		o = $({}); // object to attach pubsub events to.
	my.subscribe = function() {
		o.bind.apply(o, arguments);
	};

	my.unsubscribe = function() {
		o.unbind.apply(o, arguments);
	};

	my.publish = function() {
		o.trigger.apply(o, arguments);
	};

	my.addInitializer = function(callback) {
		var initializer = {
			scope: this,
			callback: callback
		};
		if (!initialized) {
			initializers.push(initializer);
		} else {
			initializer.callback.call(initializer.scope);
		}
	};

	my.initialize = function() {
		$.each(initializers, function(i, initializer) {
			initializer.callback.call(initializer.scope);
		});
		my.publish('after.initialize');
		// fire an event when all modules are initialized.
		initialized = true;
	};
	return my;
}(jQuery));

/**
 * Video module.
 * - Achtergrond videos
 */
var VideoModule = (function(app) {
	var my = {},
		$;
	my.name = "video";

	/**
	 *
	 **/

	function moduleInit() {
		$ = app.$;
		app.subscribe('after.initialize', _onInitializationDone);
	}

	var _onInitializationDone = function() {
		/*
			videobackground.js
			Houdt geen rekening met iOS, video gaat openen in player.
		*/
		if ( typeof jvdwBackground !== 'undefined' && jvdwBackground.hasBG ){
			if ( ! Modernizr.touch && ( Modernizr.video.h264 || Modernizr.video.ogg || Modernizr.video.webm ) ){
				var opts = {
					controlPosition: '.language_chooser',
					poster: jvdwBackground.sources.jpg,
					loadedCallback: function( that ) {
						$( this ).videobackground( 'mute' );
						var state = $.cookie( 'video_is_paused' );
						if ( state == 'pause' ) $( this ).videobackground( 'play' );
					},
					resizeTo: 'window',
					resize: true,
					loop: true,
					videoSource: []
				};
				opts.videoSource = [];
				jQuery.each( jvdwBackground.sources, function( key, source){
					if ( key != 'jpg' ){
						if( key == 'ogv' ) key = 'ogg';
						opts.videoSource.push([ source, 'video/' + key ] );
					}
				} );
				if ( ! opts.videoSource.length ) {
					// resize the poster
					$( window ).on( 'resize', function () {
						$( '.video-background' ).videobackground( 'resize' );
					});
				}
				$( 'body' ).prepend( '<div class="video-background"></div>' );
				$( '.video-background' ).videobackground(opts);
			} else {
				//log( 'touch' );
				var bg = jvdwBackground.sources.jpg;
				$( 'body' ).css( {
					'background':				'url(' + bg + ') no-repeat center center fixed',
					'-webkit-background-size':	'cover',
					'-moz-background-size':		'cover',
					'-o-background-size':		'cover',
					'background-size':			'cover'
				} );
			}

		}

		$( '.ui-video-background' ).on( 'click', 'a', function( event ){
			var c = $( this ).text() !== 'Play' ? 'play' : 'pause';
			$.cookie( 'video_is_paused', c, { expires: 7, path: '/' } );
		});
	};

	app.addInitializer(moduleInit);
	return my;
}(AppMain));

/**
 * Gallery module.
 * - Galleries
 */
var GalleryModule = (function(app) {
	var my = {},
		$;
	my.name = "gallery";

	function moduleInit() {
		$ = app.$;
		app.subscribe('after.initialize', _onInitializationDone);
	}

	var _onInitializationDone = function() {
		var gal = $( '.gallery' ),
			hasNavigation = gal.children().length > 1,
			as_options = {
				enableStartStop: false,
				hashTags: false,
				onSlideComplete     : function(slider) {
					var navPlugin = jQuery('.thumbNav').data( 'plugin_browseList' );
					navPlugin.jumptoItem( slider.currentPage );
				}
			};

		if ( hasNavigation ){
			as_options.navigationFormatter = function( i, panel ){ // add thumbnails as navigation links
				var $p = $( panel ),
					img = '<img src="' + $p.data( 'thumbnail' ) + '" class="' + $p.attr( 'class' ) + '">';
				return img;
			};
		}

		gal.anythingSlider( as_options );

		if ( hasNavigation ){
			gal.parents( '.anythingSlider' ).addClass( 'hasNavigation' );
			jQuery( '.thumbNav' ).browseList({
				loop: false,
				onInitDone: _setupNavigation,
				onDisplayDone: _setNavigationState,
				page_size: 5
			});
			var cls = 'video-with-thumb';
			$( '.' + cls ).removeClass( cls ).parents( 'a' ).addClass( cls );
		}

		$( '.gallery a' ).attr( 'rel', 'gal' ).nyroModal();
	};

	var _onDisplayDone = function( el, items ){

	};

	var _setupNavigation = function ( pluginInst, el, items ) {
		var nav_templ = app.settings.templates.nav_list.replace( '%s', 'gallery-nav');
		$nav = $( nav_templ );
		$( '.thumbNav' ).parent().append( $nav );

		var state = pluginInst.getState();

		$nav.find( '.next' ).on( 'click', function(){
			pluginInst.next();
		});
		$nav.find( '.prev' ).on( 'click', function(){
			pluginInst.prev();
		});
		_setNavigationState( pluginInst, el, items );
	};

	var _setNavigationState = function( pluginInst, el, items ){
		var next = el.parent().find( '.next' ),
			prev = el.parent().find( '.prev' ),
			state = pluginInst.getState();
		prev.toggle( state.hasPrev );
		next.toggle( state.hasNext );
	};

	app.addInitializer(moduleInit);
	return my;
}(AppMain));

/**
 * Videografie module.
 * Bladeren in de lijst met videos op de profiel paginas
 */
var VideographyModule = (function(app) {
	var my = {},
		$;
	my.name = "videography";

	function moduleInit() {
		$ = app.$;
		//app.subscribe('after.initialize', _onInitializationDone);
		$( window ).bind('load', _onInitializationDone );
	}

	// 'private' method.
	var _onInitializationDone = function() {
		var nav_templ = app.settings.templates.nav_list.replace( '%s', 'video-nav' );
		jQuery( '.video-list' ).after( nav_templ ).browseList( {
			loop: false,
			onInitDone: _setupNavigation,
			beforeElementDisplay: _beforeElementDisplay,
			onDisplayDone: _setNavigationState,
			page_size: 5
		} );
	};

	var _beforeElementDisplay = function( el ){
		var iframe = el.find( 'iframe' );
		iframe.attr( 'src',  iframe.data( 'src' ) );
	};

	var _onDisplayDone = function( el, items ){

	};

	var _setupNavigation = function ( pluginInst, el, items ) {
		var state = pluginInst.getState();
		el.next().find( '.next' ).on( 'click', function(){
			pluginInst.next();
		});
		el.next().find( '.prev' ).on( 'click', function(){
			pluginInst.prev();
		});
		_setNavigationState( pluginInst, el, items );
	};

	var _setNavigationState = function( pluginInst, el, items ){
		var next = el.next().find( '.next' ),
			prev = el.next().find( '.prev' ),
			state = pluginInst.getState();
		prev.toggle( state.hasPrev );
		next.toggle( state.hasNext );
	};

	app.addInitializer(moduleInit);
	return my;
}(AppMain));

/**
 * News items list module.
 * Voegt een paar 'fake' nieuws items in de lijst zodat ie
 * random verspringt.
 */
var NewsItemsModule = (function(app) {
	var my = {},
		$;
	my.name = "newsitems";

	function moduleInit() {
		$ = app.$;
		if ( $( 'body' ).hasClass( 'home-news' ) ) {
			app.subscribe('after.initialize', _onInitializationDone);
		}
	}

	var _onInitializationDone = function() {
		var items	= $( 'article.news' ),
			item	= '<article class="news fake"></article>',
			insert	= 3,
			idx		= 0, rep = 0,
			h		= items.eq( 0 ).height();

		for( var i = 0; i < insert; i++ ){
			if ( getRandomInt( 0, 1 ) && rep < 2 ) {
				items.eq( idx ).before( $( item ).height( h ) );
				rep++;
			} else {
				idx++;
				rep = 0;
			}
		}
	};

	var getRandomInt = function ( min, max ) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	app.addInitializer(moduleInit);
	return my;
}(AppMain));

// Start it all up.
jQuery(function() {
	'use strict';
	AppMain.initialize();
});