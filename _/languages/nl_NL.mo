��    +      t  ;   �      �     �     �     �     �     �     �                =     B     S  $   b  	   �     �     �  	   �     �     �     �     �     �     �          +     F  D   U     �     �     �     �     �     �  A   �  ,   ;      h  	   �     �  ;   �     �     �            �  "     �     	     	     +	     :	     L	     b	     t	     �	     �	     �	      �	     �	     �	     �	      
     
     
     '
     7
     O
  ,   n
     �
  %   �
     �
  `   �
     O  	   ]     g     x     �     �  9   �  4   �  ,        H     V  %   e  %   �     �     �     �            '               	      !       #                             "       %                 )      (   
                                       $   +                                *                 &    Add New News Add New Project All News All Projects All Settings Archives by category Archives by month BG_VIDEO_DIR does not exist. Back Background video Choose a video Comments are disabled for this post. Edit News Edit Project Filmography More news New News New Project Next project No articles found No news items found No news items found in Trash No projects found No projects found in Trash Page not found Please set BG_VIDEO_DIR in theme options before using this function. Previous project Projects Read the article Return to the home page Search News Search Projects The directory does not exist. Please remember create it manually. The page you are looking for does not exist. This post is password-protected. View News View Project You do not have sufficient permissions to access this page. Your theme settings were saved. in development in production realized Project-Id-Version: JvdW
POT-Creation-Date: 2013-01-21 23:48+0100
PO-Revision-Date: 2013-01-21 23:49+0100
Last-Translator: 
Language-Team: 
Language: EN_en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: /Volumes/Data/Development/web/tv/jvdw/www/wp-content/themes/jvdw
 Voeg nieuw Nieuws toe Voeg nieuw Project toe Alle Nieuws Alle Projecten Alle Instellingen Archief per categorie Archief per maand BG_VIDEO_DIR bestaat niet. Terug Achtergrond Video Kies een video U kan geen reacties achterlaten. Bewerk Nieuws Bewerk Project Filmografie Meer nieuws Nieuw Nieuws Nieuw Project Volgend project Geen artikelen gevonden Geen Nieuws berichten gevonden Geen Nieuws berichten gevonden in de Vuilnis Geen Projecten gevonden Geen Projecten gevonden in de Vuilnis Pagina niet gevonden Vul a.u.b. eerst de BG_VIDEO_DIR variabele in in de Theme Opties voordat u deze functie gebruikt Vorig project Projecten Lees het artikel Terug naar de voorpagina Zoek Nieuws Zoek Projecten De directory bestaat niet. Maak hem a.u.b. handmatig aan. De pagina waarnaar u zoekt kan niet gevonden worden. Deze pagina is met een wachtwoord beschermd. Bekijk Nieuws Bekijk Project U heeft geen toegang tot deze pagina. U theme instellingen zijn opgeslagen. in ontwikkeling in productie gerealiseerd 