/*global module:false*/
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Project configuration.
  grunt.initConfig({
    meta: {
      version: '0.1.0',
      banner: '/*! Some Project - v<%= 1.0 %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '* http://www.jvdwfilm.nl/\n' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
        'Heerko van der Kooij; Licensed MIT */'
    },
    lint: {
      files: ['_/js/theme.js']
    },
    qunit: {
      files: ['test/**/*.html']
    },
    concat: {
      dist: {
        src: [
          '<banner:meta.banner>',
          '_/js/libs/*.js',
          '_/js/libs/anythingslider/js/jquery.anythingslider.js',
          '_/js/libs/anythingslider/js/jquery.anythingslider.video.js',
          '_/js/libs/videobackground/script/jquery.videobackground.js',
          '_/js/libs/jquery.nyroModal/js/jquery.nyroModal.custom.js',
          '_/js/custom/*.js',
          '<file_strip_banner:_/js/theme.js>'],
        dest: '_/js/dist/theme.concat.js'
      }
    },
    min: {
      dist: {
        src: ['<banner:meta.banner>', '<config:concat.dist.dest>'],
        dest: '_/js/dist/theme.min.js'
      }
    },
    watch: {
      files: [ '<config:lint.files>', '_/sass/*.scss','_/sass/**/*.scss', '_/js/*.js','_/js/**/*.js' ], //, '*.php', '**/*.php'
      tasks: [ 'compass:dev', 'lint' ]
    },
    jshint: {
      options: {
        // curly: true,
        // eqeqeq: true,
        // immed: true,
        // latedef: true,
        // newcap: true,
        // noarg: true,
        // sub: true,
        // undef: true,
        // boss: true,
        // eqnull: true,
        debug: true,
        browser: true,
        devel: true,
        smarttabs: true
      },
      globals: {
        jQuery: true,
        _: true,
        ThemeGlobal: true,
        log: true
      }
    },
    uglify: {},
    compass: {
      dev: {
          // src: '_/sass',
          // dest: '_/css',
          // linecomments: true,
          // forcecompile: false,
          // require: 'animate-sass mylib',
          // debugsass: true,
          // images: '_/img',
          // relativeassets: true
          config: 'config.rb'
      },
      prod: {
          src: '_/sass',
          dest: '_/css',
          outputstyle: 'compressed',
          linecomments: false,
          forcecompile: true,
          // require: 'animate-sass mylib',
          debugsass: false,
          images: '_/img',
          relativeassets: true
      }
    }
  });

  // Default task.
  grunt.registerTask('default', 'lint concat min');
  grunt.registerTask('prod', 'compass:prod concat min');//'compass:prod lint concat min');
};
