<?php
require_once("Database.php");
$db = new Database();
$version = isset( $_GET[ 'version' ] ) ? $_GET[ 'version' ] : 1;

if ( $_GET[ 'mode' ] == 'get' ) {
	$punten = getPunten( $version );
	$response = array(
		'punten' => $punten
	);
	printResponse( $response );
} else if ( $_GET[ 'mode' ] == 'put' ) {
	$punt = isset( $_GET[ 'punt' ] ) ? validatePunt( $_GET[ 'punt' ] ) : '';
	if ( empty( $punt ) ){
		exit( json_encode( array( 'error' => true, 'input' => $_GET[ 'punt' ], 'validated' => $punt ) ) );
	}
	$punten = truncatePunten( getPunten( $version ) );
	$punten .= '|' . $punt;
	$sql = "UPDATE `punten` as p SET p.punten='$punten'";
	$db->query( $sql );
	$response = array(
		'error' => 0,
		'punten' => getPunten( $version )
	);
	printResponse( $response );
}

function getPunten( $version ){
	global $db;
	$sql = "SELECT * FROM `punten` WHERE `site_version`='$version' ";
	$db->query( $sql );
	$db->singleRecord();
	return $db->Record[ 'punten' ];
}

function validatePunt( $punt ){
	$punt = urldecode( $punt );
	$matches = array();
	$found = preg_match ( "/^(-?\d+),(-?\d)+$/" , $punt, $matches );
	if ( $found < 1 ) {
		$punt = null;
	}
	// echo "return : " . $punt;
	return $punt;
}

function truncatePunten( $punten ){
	$arr = explode( '|', $punten );
	$length = count( $arr );
	$max_lenght = 100;
	if ( $length > $max_lenght ){
		$arr = array_splice( $arr, $length - $max_lenght );
		$punten = implode( '|', $arr );
	}
	return $punten;
}

function printResponse( $response ){
	// header( 'Cache-Control: no-cache, must-revalidate' );
	// header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
	// header( 'Content-type: application/json' );
	echo json_encode( $response );
}
/* eof */