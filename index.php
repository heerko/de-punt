<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>De Punt</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="_/css/screen.css">
        <script src="_/js/lib/modernizr/modernizr-2.6.2.min.js"></script>
        <link rel="author" href="humans.txt" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    </head>
    <body>
        <div class="punt">de punt</div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="_/js/lib/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="_/js/app/theme.js"></script>
    </body>
</html>